package edu.app.iso.jUnitTest;





import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.Test;

import edu.app.iso.delegate.EnterpriseServiceDelegate;
import edu.app.iso.persistence.Enterprise;

public class EnterpriseTest {
	

	

	
	@Test
public void itShouldCreateAnEnterprise(){

	Enterprise toCreate= new Enterprise("",null,null,"test",null);
	EnterpriseServiceDelegate.createEnterprise(toCreate);
}

@Test
public void itShouldFindAnEnterprise(){
	Enterprise toFind=EnterpriseServiceDelegate.findEnterprise(11);
	assertNotNull(toFind);

}

@Test
public void itShouldUpdateAnEnterprise(){
	Enterprise toUpdate=new Enterprise("","testU",null,"test",null);
	EnterpriseServiceDelegate.updateEnterprise(toUpdate);
	
}
@Test
public void itShouldRemoveAnEnterprise(){
	Enterprise toRemove=new Enterprise("","testR",null,"test",null);
	
	EnterpriseServiceDelegate.removeEnterprise(toRemove);
}

@Test
public void itShouldFindAllEnterprises(){
	List<Enterprise> Found =EnterpriseServiceDelegate.findAllEnterprises();
	assertNotNull(Found);
}
@After
public void clean(){
	for(Enterprise ent: EnterpriseServiceDelegate.findAllEnterprises())
		EnterpriseServiceDelegate.removeEnterprise(ent);
	
}

}
