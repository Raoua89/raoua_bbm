package edu.app.iso.jUnitTest;




import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.Test;

import edu.app.iso.delegate.ExpertServiceDelegate;
import edu.app.iso.persistence.Expert;


public class ExpertTest {

	
	
	@Test
	public void itShouldCreateAnExpert(){
	
	    Expert toCreate= new Expert("testC","testC","testC","test@test.com",1);
		ExpertServiceDelegate.createExpert(toCreate);
	}

@Test
public void itShouldFindAnExpert(){
	Expert toFind=ExpertServiceDelegate.findExpert("testF");
	assertNotNull(toFind);


}

@Test
public void itShouldUpdateAnExpert(){
	Expert toUpdate=new Expert("testU","testU","testU","test@test.com",1);
	ExpertServiceDelegate.updateExpert(toUpdate);
	
}

@Test
public void itShouldRemoveAnExpert(){
	Expert toRemove=new Expert("testR","testR","testR","test@test.com",1);
	ExpertServiceDelegate.removeExpert(toRemove);

}

@Test
public void itShouldFindAllAdmins(){
	List<Expert> Found =ExpertServiceDelegate.findAllExperts();
	assertNotNull(Found);
			
	}
@After
public void clean(){
	for(Expert exp: ExpertServiceDelegate.findAllExperts())
		ExpertServiceDelegate.removeExpert(exp);
	
}

}
