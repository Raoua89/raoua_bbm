package edu.app.iso.jUnitTest;


import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import edu.app.iso.delegate.AdminServiceDelegate;
import edu.app.iso.persistence.Administrator;

public class AdminTest {
	


	@Test
public void itShouldCreateAnAdmin(){
    Administrator toCreate= new Administrator("testC","testC","testC","test@test.com",1);
	AdminServiceDelegate.createAdmin(toCreate);
}

@Test
public void itShouldFindAnAdmin(){
	Administrator toFind=AdminServiceDelegate.findAdmin("testF");
	assertNotNull(toFind);

	
}



@Test
public void itShouldNotFindInexistantAdmin(ExpectedException Exception){
	Administrator toFind=AdminServiceDelegate.findAdmin("x");
	assertNotNull(toFind);

	
}

@Test
public void itShouldUpdateAnAdmin(){
	Administrator toUpdate=new Administrator("testU","testU","testU","testU@testU.com",22629320);
	AdminServiceDelegate.updateAdmin(toUpdate);
	
		
}
@Test
public void itShouldRemoveAnAdmin(){
	Administrator toRemove=new Administrator("testR","testR","testR","test@test.com",1);
	AdminServiceDelegate.removeAdmin(toRemove);

}

@Test
public void itShouldFindAllAdmins(){
	List<Administrator> Found =AdminServiceDelegate.findAllAdmins();
	assertNotNull(Found);
			
	}
@After
public void clean(){
	for(Administrator admin: AdminServiceDelegate.findAllAdmins())
		AdminServiceDelegate.removeAdmin(admin);
	
}

}
