package edu.app.iso.jUnitTest;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.Test;

import edu.app.iso.delegate.EmployeeServiceDelegate;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Enterprise;

public class EmployeeTest {
	
	
	
	Enterprise entTest= new Enterprise();
	
	
	
	

		
		@Test
	public void itShouldCreateAnEmployee(){
		
		Employee toCreate=new Employee("testC","testC","testC","test@test.com",1,1," ",null);
		
		EmployeeServiceDelegate.createEmployee(toCreate);
	}

	@Test
	public void itShouldFindAnEmployee(){
		Employee toFind=null;
		toFind=EmployeeServiceDelegate.findEmployee("testF");
		assertNotNull(toFind);

     }

	@Test
	public void itShouldUpdateAnEmployee(){
		Employee toUpdate=new Employee("testU","testU","testU","test@test.com",1,1," ",null);
		EmployeeServiceDelegate.updateEmployee(toUpdate);
	}
	
	@Test
	public void itShouldFindAllEmployees(){
		
		List<Employee> Found =EmployeeServiceDelegate.findAllEmployees(entTest);
		assertNotNull(Found);
				
		}

	@Test
	public void itShouldRemoveAnEmployee(){
		Employee toRemove=new Employee("testR","testR","testR","test@test.com",1,1," ",null);
		EmployeeServiceDelegate.removeEmployee(toRemove);
	
	}
	
	@After
	public void clean(){
		for(Employee emp: EmployeeServiceDelegate.findAllEmployees(entTest))
			EmployeeServiceDelegate.removeEmployee(emp);
		
	}


}
