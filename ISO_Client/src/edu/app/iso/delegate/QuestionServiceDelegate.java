package edu.app.iso.delegate;

import java.util.List;

import edu.app.iso.business.QuestionServiceRemote;
import edu.app.iso.persistence.Course;
import edu.app.iso.persistence.QCM;
import edu.app.iso.persistence.Question;
import edu.app.iso.persistence.Response;
import edu.app.iso.util.ServiceLocator;

public class QuestionServiceDelegate {
	
	private static QuestionServiceRemote getProxy(){
        return (QuestionServiceRemote) ServiceLocator.lookup("QuestionService", QuestionServiceRemote.class);
}
	
	
	
	

	public static void createQuestion(Question question) {
		getProxy().create(question);
		
	}

	public static Question findQuestion(int id) {
		return getProxy().find(id);
	}

	public static void updateQuestion(Question question) {
		getProxy().edit(question);
		
	}

	public static void removeQuestion(Question question) {
		getProxy().remove(question);
	}

	public static List<Question> findAllQuestions(Course course, String dificulty) {
		return getProxy().findQuestions(course,dificulty);
	}
	
	public static Question chooseQuestion(List<Question> list){
		return getProxy().chooseQuestion(list);
	}

	public static Response fetchResponse(Question question, QCM qcm){
		return getProxy().fetchResponse(question,qcm);
	}





	public static List<Question> findQuestions(Course course) {
		
		return  getProxy().findAllQuestions(course);
	}

	

}
