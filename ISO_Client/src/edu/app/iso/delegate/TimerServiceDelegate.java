package edu.app.iso.delegate;

import edu.app.iso.timer.MyTimerRemote;
import edu.app.iso.util.ServiceLocator;

public class TimerServiceDelegate {
	
	private static MyTimerRemote getProxy(){
        return (MyTimerRemote) ServiceLocator.lookup("TimerService", MyTimerRemote.class);
}
	
	
	
	

	public static void start(int time) {
		getProxy().start(time);
		
	}

}
