package edu.app.iso.delegate;

import java.util.List;

import edu.app.iso.business.ResponseServiceRemote;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Question;
import edu.app.iso.persistence.Response;
import edu.app.iso.util.ServiceLocator;

public class ResponseServiceDelegate {
	
	private static ResponseServiceRemote getProxy(){
        return (ResponseServiceRemote) ServiceLocator.lookup("ResponseService", ResponseServiceRemote.class);
}

	public static void createResponse(Response response) {
		getProxy().create(response);
		
	}

	public static Response findResponse(int id) {
		return getProxy().find(id);
	}

	public static void updateResponse(Response response) {
		getProxy().edit(response);
		
	}

	public static void removeResponse(Response response) {
		getProxy().remove(response);
	}

	public static List<Response> findAllResponses(Question question) {
		return getProxy().findAll(question);
	}
	
	public static List<Response> findResponsesByEmployee(Question question,Employee employee) {
		return getProxy().findResponses(question,employee);
	}


}
