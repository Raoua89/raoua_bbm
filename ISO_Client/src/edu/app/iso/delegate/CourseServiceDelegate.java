package edu.app.iso.delegate;

import java.util.List;

import edu.app.iso.business.CourseServiceRemote;
import edu.app.iso.persistence.BusinessArea;
import edu.app.iso.persistence.Course;
import edu.app.iso.util.ServiceLocator;

public class CourseServiceDelegate {
	
	private static CourseServiceRemote getProxy(){
        return (CourseServiceRemote) ServiceLocator.lookup("CourseService", CourseServiceRemote.class);
    }

	public static void createCourse(Course course) {
		getProxy().create(course);
		
	}

	public static Course findCourse(int id) {
		return getProxy().find(id);
	}
	
	public static Course findByTitle(String title) {
		return getProxy().findByTitle(title);
	}

	public static void updateCourse(Course course) {
		getProxy().edit(course);
		
	}

	public static void removeCourse(Course course) {
		getProxy().remove(course);
	}

	public static List<Course> findAllCourses() {
		return getProxy().findAll();
	}
	
	public static List<Course> findByBusinessArea(BusinessArea bus) {
		return getProxy().findByBusinessArea(bus.toString());
	}


	
	

}
