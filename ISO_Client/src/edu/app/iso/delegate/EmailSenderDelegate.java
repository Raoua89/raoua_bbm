package edu.app.iso.delegate;

import edu.app.iso.business.EmailSenderRemote;

import edu.app.iso.util.ServiceLocator;

public class EmailSenderDelegate {
	
	private static EmailSenderRemote getProxy(){
        return (EmailSenderRemote) ServiceLocator.lookup("EmailSender", EmailSenderRemote.class);
        }

	public static void sendEMail(String sender, String receiver,String text) {
		getProxy().sendEMail( sender, receiver,text);
	
	}

}
