package edu.app.iso.delegate;

import java.util.List;

import edu.app.iso.business.IsoUserServiceRemote;
import edu.app.iso.persistence.Administrator;
import edu.app.iso.util.ServiceLocator;

public class AdminServiceDelegate {
	
	
	
	@SuppressWarnings("unchecked")
	private static IsoUserServiceRemote<Administrator> getProxy(){
         return (IsoUserServiceRemote<Administrator>) ServiceLocator.lookup("AdminService", IsoUserServiceRemote.class);
 }

	public static void createAdmin(Administrator admin) {
		getProxy().create(admin);
	
	}

	public static Administrator findAdmin(String login) {
	
		return getProxy().find(login);
	}

	public static void updateAdmin(Administrator admin) {
		
		getProxy().edit(admin);
	}

	public static void removeAdmin(Administrator admin) {
		getProxy().remove(admin);
		
	}

	public static List<Administrator> findAllAdmins() {
		
		return getProxy().findAll();
	}

	public static Administrator authenticateAdmin(String login, String password) {
	
		return getProxy().authenticate(login, password);
	}

}
