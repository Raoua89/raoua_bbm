package edu.app.iso.delegate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.app.iso.business.QcmServiceRemote;
import edu.app.iso.persistence.Course;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.QCM;
import edu.app.iso.persistence.Question;
import edu.app.iso.persistence.Score;
import edu.app.iso.util.ServiceLocator;

public class QcmServiceDelegate {


	private static QcmServiceRemote getProxy(){
        return (QcmServiceRemote) ServiceLocator.lookup("QcmService", QcmServiceRemote.class);
    }

	public static void createQcm(QCM qcm) {
		getProxy().create(qcm);
		
	}
	
	
	public static void updateNbQuestion(int nb) {
		getProxy().updateNbQuestion(nb);
		
	}
	
	public static int getNbQuestion() {
		return getProxy().getNbQuestion();
		
	}

	public static QCM findQcm(int id) {
		return getProxy().find(id);
	}

	public static void updateQcm(QCM qcm) {
		getProxy().edit(qcm);
		
	}

	public static void removeQcm(QCM qcm) {
		getProxy().remove(qcm);
	}

	public static List<QCM> findAllQcms() {
		return getProxy().findAll();
	}
	
	
	public static List<QCM> findByEmployee(Employee emp) {
		List<QCM> result = new ArrayList<QCM>();
		for(Score s: ScoreServiceDelegate.findAllScores(emp))
		result.add(s.getQcm());
		return result;
	}
	
	public static void calculateScore(QCM qcm) {
	   getProxy().CalculFinalScore(qcm);
	}
	
	public static QCM prepareQcm(String difficulty,Employee emp,Course course, Date date) {
		QCM qcm=new QCM(emp,difficulty,date,0);
             QcmServiceDelegate.createQcm(qcm);

       List<Question> availableQuestions =QuestionServiceDelegate.findAllQuestions(course,difficulty) ;
		      for(int i=0 ; i< QCM.getNbQuestions();i++){
			        Question quest= new Question();
                    quest= QuestionServiceDelegate.chooseQuestion(availableQuestions);
			        Score score= new Score(qcm,quest,0);
			        ScoreServiceDelegate.createScore(score);
			        qcm.addScore(score);
		      }
       QcmServiceDelegate.updateQcm(qcm);
       return qcm;
	}

	public static Question fetchQuestion(QCM qcm, int i) {
		
		return qcm.getScores().get(i).getQuestion();
	}
	
	


}
