package edu.app.iso.delegate;

import java.util.List;
import edu.app.iso.business.IsoEnterpriseServiceRemote;
import edu.app.iso.persistence.Enterprise;
import edu.app.iso.util.ServiceLocator;

public class EnterpriseServiceDelegate {
	
	
	private static IsoEnterpriseServiceRemote getProxy(){
        return (IsoEnterpriseServiceRemote) ServiceLocator.lookup("EnterpriseService", IsoEnterpriseServiceRemote.class);
}

	public static void createEnterprise(Enterprise enterprise) {
		getProxy().createEnterprise(enterprise);
		
	}
;
	public static Enterprise findEnterprise(int id) {
		return getProxy().findEnterprise(id);
		
	}

	public static Enterprise findEnterpriseByName(String name) {
		return getProxy().findEnterpriseByName(name);
	}

	public static void updateEnterprise(Enterprise enterprise) {
	getProxy().updateEnterprise(enterprise);
		
	}

	public static void removeEnterprise(Enterprise enterprise) {
		getProxy().removeEnterprise(enterprise);
	}

	public static List<Enterprise> findAllEnterprises() {
	return getProxy().findAllEnterprises();
	}

}
