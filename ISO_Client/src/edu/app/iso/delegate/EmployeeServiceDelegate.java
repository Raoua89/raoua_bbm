package edu.app.iso.delegate;
import java.util.List;

import edu.app.iso.business.IsoUserServiceRemote;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Enterprise;
import edu.app.iso.util.ServiceLocator;

public class EmployeeServiceDelegate{
	
	
	@SuppressWarnings("unchecked")
	private static IsoUserServiceRemote<Employee> getProxy(){
        return (IsoUserServiceRemote<Employee>) ServiceLocator.lookup("EmployeeService", IsoUserServiceRemote.class);
}

	public static void createEmployee(Employee emp) {
	getProxy().create(emp);
		
	}

	
	public static Employee findEmployee(String login) {
		return getProxy().find(login);
	}

	
	public static void updateEmployee(Employee emp) {
		getProxy().edit(emp);
		
	}

	
	public static void removeEmployee(Employee emp) {
		getProxy().remove(emp);
	}

	
	public static List<Employee> findAllEmployees(Enterprise ent) {
		return getProxy().findAll(ent);
	}


	public static Employee authenticateEmployee(String login, String password) {
	    return getProxy().authenticate(login, password);
	}

}
