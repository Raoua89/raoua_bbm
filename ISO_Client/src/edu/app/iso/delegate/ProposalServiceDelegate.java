package edu.app.iso.delegate;

import java.util.List;

import edu.app.iso.business.ProposalServiceRemote;
import edu.app.iso.persistence.Proposal;
import edu.app.iso.persistence.Question;
import edu.app.iso.util.ServiceLocator;

public class ProposalServiceDelegate {
	
	
	private static ProposalServiceRemote getProxy(){
        return (ProposalServiceRemote) ServiceLocator.lookup("ProposalService", ProposalServiceRemote.class);
}

	public static void createProposal(Proposal proposal) {
		getProxy().create(proposal);
		
	}

	public static Proposal findProposal(int id) {
		return getProxy().find(id);
	}

	public static void updateProposal(Proposal proposal) {
		getProxy().edit(proposal);
		
	}

	public static void removeProposal(Proposal proposal) {
		getProxy().remove(proposal);
	}

	public static List<Proposal> findAllProposals() {
		return getProxy().findAllProposals();
	}
	
	public static List<Proposal> findProposals(Question question){
		return getProxy().findProposals(question);
	}


}
