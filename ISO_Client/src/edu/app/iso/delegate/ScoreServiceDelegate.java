package edu.app.iso.delegate;

import java.util.List;

import edu.app.iso.business.ScoreServiceRemote;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Score;
import edu.app.iso.persistence.ScorePK;
import edu.app.iso.util.ServiceLocator;

public class ScoreServiceDelegate {
	
	
	private static ScoreServiceRemote getProxy(){
        return (ScoreServiceRemote) ServiceLocator.lookup("ScoreService", ScoreServiceRemote.class);
    }

	public static void createScore(Score score) {
		getProxy().create(score);
		
	}

	public static Score findScore(ScorePK pk) {
		return getProxy().find(pk);
	}

	public static void updateScore(Score score) {
		getProxy().edit(score);
		
	}

	public static void removeScore(Score score) {
		getProxy().remove(score);
	}
	
	public static List<Score> findAllScores(Employee emp) {
		return getProxy().findAllScores(emp);
	}

}
