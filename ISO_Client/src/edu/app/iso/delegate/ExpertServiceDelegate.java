package edu.app.iso.delegate;

import java.util.List;
import edu.app.iso.business.IsoUserServiceRemote;
import edu.app.iso.persistence.Expert;
import edu.app.iso.util.ServiceLocator;

public class ExpertServiceDelegate{
	
	@SuppressWarnings("unchecked")
	private static IsoUserServiceRemote<Expert> getProxy(){
        return (IsoUserServiceRemote<Expert>) ServiceLocator.lookup("ExpertService", IsoUserServiceRemote.class);
}

	public static void createExpert(Expert exp) {
		getProxy().create(exp);
		
	}

	public static Expert findExpert(String login) {
		return getProxy().find(login);
	}

	public static void updateExpert(Expert exp) {
		getProxy().edit(exp);
		
	}

	public static void removeExpert(Expert exp) {
		getProxy().remove(exp);
	}

	public static List<Expert> findAllExperts() {
		return getProxy().findAll();
	}

	public static Expert authenticateExpert(String login, String password) {
		
		return getProxy().authenticate(login, password);
	}

}
