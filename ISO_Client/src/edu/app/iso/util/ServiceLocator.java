package edu.app.iso.util;



	import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

	public class ServiceLocator {
		static Context ctx;
		static Map<String, Object> cache=new HashMap<String, Object>();
		
		public static Object lookup (String serviceName, Class<?> serviceViewClass) {
		
		
		Object o=null;
		try {
			if(ctx==null)
			ctx=new InitialContext();
			
			
			if(cache.containsKey(serviceName)){
				o=cache.get(serviceName);
			}
			else{
			
			
			String viewClass=serviceViewClass.getCanonicalName();
			o= ctx.lookup("ISO_Platform//"+serviceName+"!"+viewClass);
			
			cache.put(serviceName, o);
			}
		}
		catch (NamingException e){
			e.printStackTrace();
		}
		return o;
		}
	}



