package edu.app.iso.gui;


import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import edu.app.iso.delegate.EmployeeServiceDelegate;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Enterprise;

public class EmployeesListFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private List<Employee> employees;
	private JTable table;
	
	
	
	
	
	

	/**
	 * Create the frame.
	 * 
	 */
	public EmployeesListFrame(Enterprise enterprise) {
		
		employees= EmployeeServiceDelegate.findAllEmployees(enterprise);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel masterPanel = new JPanel();
		masterPanel.setBorder(new TitledBorder(null, "Slect an employee to see its details", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(masterPanel, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(masterPanel, GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_masterPanel = new GroupLayout(masterPanel);
		gl_masterPanel.setHorizontalGroup(
			gl_masterPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_masterPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_masterPanel.setVerticalGroup(
			gl_masterPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_masterPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		masterPanel.setLayout(gl_masterPanel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Employee, List<Employee>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, employees, table);
		//
		BeanProperty<Employee, String> employeeBeanProperty = BeanProperty.create("login");
		jTableBinding.addColumnBinding(employeeBeanProperty).setColumnName("Login:").setEditable(false);
		//
		BeanProperty<Employee, String> employeeBeanProperty_1 = BeanProperty.create("name");
		jTableBinding.addColumnBinding(employeeBeanProperty_1).setColumnName("Name").setEditable(false);
		//
		BeanProperty<Employee, String> employeeBeanProperty_2 = BeanProperty.create("speciality");
		jTableBinding.addColumnBinding(employeeBeanProperty_2).setColumnName("speciality:").setEditable(false);
		//
		
		table.setCellSelectionEnabled(true);
		ListSelectionModel cellSelectionModel = table.getSelectionModel();
		cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {

		String selectedData = null;
		int selectedRow = table.getSelectedRow ();
		selectedData = (String) table.getValueAt(selectedRow,0);

		
		 System.out.println(selectedData);
		final Employee selectedEmployee=EmployeeServiceDelegate.findEmployee(selectedData);
		
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
			new EmployeeDetailsFrame(selectedEmployee).setVisible(true);
			}
			});	
		




		}
		});
		
		
		jTableBinding.bind();
	}
}
