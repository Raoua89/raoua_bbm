package edu.app.iso.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.ExpertServiceDelegate;
import edu.app.iso.persistence.Expert;
import edu.app.iso.session.Session;

public class ExpertAccountFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField loginTextField;
	private JTextField nameTextField;
	private JTextField eMailTextField;
	private JTextField phoneNumberTextField;

	

	/**
	 * Create the frame.
	 */
	public ExpertAccountFrame() {
		
		final Expert expert=(Expert) Session.getInstance().getParametre("AUTH_USER");
		setTitle("Expert Account");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 659, 698);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("Login");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_1 = new JLabel("Name");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_2 = new JLabel("Email");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblNewLabel_3 = new JLabel("Phone number");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		loginTextField = new JTextField();
		loginTextField.setColumns(10);
		loginTextField.setText(expert.getLogin());
		
		nameTextField = new JTextField();
		nameTextField.setColumns(10);
		nameTextField.setText(expert.getName());
		
		eMailTextField = new JTextField();
		eMailTextField.setColumns(10);
		eMailTextField.setText(expert.getEmail());
		
		phoneNumberTextField = new JTextField();
		phoneNumberTextField.setColumns(10);
		phoneNumberTextField.setText(Integer.toString(expert.getPhoneNumber()));
		
		JButton updateBtn = new JButton("");
		updateBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Expert toUpdate = new Expert(expert.getLogin(),expert.getPassword(),nameTextField.getText(),eMailTextField.getText(),Integer.parseInt(phoneNumberTextField.getText()));
				ExpertServiceDelegate.updateExpert(toUpdate);
				
			}
		});
		updateBtn.setIcon(new ImageIcon(ExpertAccountFrame.class.getResource("/edu/app/iso/icone/EDIT\u00E0a.jpg")));
		
		JButton removeBtn = new JButton("");
		removeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ExpertServiceDelegate.removeExpert(expert);
				
			}
		});
		removeBtn.setIcon(new ImageIcon(ExpertAccountFrame.class.getResource("/edu/app/iso/icone/Delet.png")));
		
		JButton backBtn = new JButton("");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ExpertAccountFrame.this.dispose();
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new ExpertFrame().setVisible(true);
					}
					});
			}
		});
		backBtn.setIcon(new ImageIcon(ExpertAccountFrame.class.getResource("/edu/app/iso/icone/Back00.jpg")));
		
		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setIcon(new ImageIcon(ExpertAccountFrame.class.getResource("/edu/app/iso/icone/Expert details.png")));
		
		JButton editPasswordBtn = new JButton("change my password");
		editPasswordBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new EditPasswordFrame().setVisible(true);
					}
					});
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(242)
					.addComponent(lblNewLabel_4, GroupLayout.PREFERRED_SIZE, 283, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(108, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(52)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblNewLabel_1)
									.addGap(103)
									.addComponent(nameTextField, GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblNewLabel_2)
									.addGap(103)
									.addComponent(eMailTextField, GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblNewLabel_3)
									.addGap(32)
									.addComponent(phoneNumberTextField, GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblNewLabel)
									.addGap(106)
									.addComponent(loginTextField, GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE))
								.addComponent(editPasswordBtn, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
							.addGap(147)
							.addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)))
					.addGap(152)
					.addComponent(removeBtn, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
					.addGap(41))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(lblNewLabel_4)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(loginTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(54)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(nameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(52)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_2)
						.addComponent(eMailTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(55)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_3)
						.addComponent(phoneNumberTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(54)
					.addComponent(editPasswordBtn)
					.addGap(41)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
							.addGap(26))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(removeBtn, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
								.addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE))
							.addGap(47))))
		);
		contentPane.setLayout(gl_contentPane);
	}

}
