package edu.app.iso.gui;


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.AdminServiceDelegate;
import edu.app.iso.persistence.Administrator;
import edu.app.iso.session.Session;
public class AdminAccountFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField loginTextField;
	private JTextField nameTextField;
	private JTextField eMailTextField;
	private JTextField phoneNumberTextField;
	
	
	/*

	/**
	 * Create the frame.
	 * @param admin 
	 */
	public AdminAccountFrame() {
		
		final Administrator admin=(Administrator) Session.getInstance().getParametre("AUTH_USER");
		
		setTitle("Admin Details");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 691, 687);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("Login");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_1 = new JLabel("");
		
		JLabel lblNewLabel_2 = new JLabel("name");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_3 = new JLabel("Email");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_4 = new JLabel("Phone number ");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		loginTextField = new JTextField();
		loginTextField.setColumns(10);
		loginTextField.setText(admin.getLogin());
		
		nameTextField = new JTextField();
		nameTextField.setColumns(10);
		nameTextField.setText(admin.getName());
		
		eMailTextField = new JTextField();
		eMailTextField.setColumns(10);
		eMailTextField.setText(admin.getEmail());
		
		phoneNumberTextField = new JTextField();
		phoneNumberTextField.setColumns(10);
		phoneNumberTextField.setText(Integer.toString(admin.getPhoneNumber()));
		
		
		JButton updateBtn = new JButton("");
		updateBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator toUpdate = new Administrator(admin.getLogin(),admin.getPassword(),nameTextField.getText(),eMailTextField.getText(),Integer.parseInt(phoneNumberTextField.getText()));
				AdminServiceDelegate.updateAdmin(toUpdate);
				
			}
		});
		updateBtn.setIcon(new ImageIcon(AdminAccountFrame.class.getResource("/edu/app/iso/icone/EDIT\u00E0a.jpg")));
		
		JButton removeBtn = new JButton("");
		removeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 JOptionPane.showConfirmDialog(new javax.swing.JDialog(),"do you really want to delete this administrator ? !!!", "warning",JOptionPane.OK_CANCEL_OPTION);
				AdminServiceDelegate.removeAdmin(admin);
			}
		});
		removeBtn.setIcon(new ImageIcon(AdminAccountFrame.class.getResource("/edu/app/iso/icone/Delet.png")));
		
		JButton backBtn = new JButton("");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminAccountFrame.this.dispose();
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new AdminFrame().setVisible(true);
					}
					});
			}
		});
		backBtn.setIcon(new ImageIcon(AdminAccountFrame.class.getResource("/edu/app/iso/icone/Back00.jpg")));
		
		JLabel lblNewLabel_5 = new JLabel("");
		lblNewLabel_5.setIcon(new ImageIcon(AdminAccountFrame.class.getResource("/edu/app/iso/icone/Admin Details.png")));
		
		JButton changPasswordBtn = new JButton("change my password");
		changPasswordBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new EditPasswordFrame().setVisible(true);
					}
					});
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
					.addGap(151)
					.addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 206, Short.MAX_VALUE)
					.addComponent(removeBtn, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
					.addGap(42))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(100)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(changPasswordBtn)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel_1)
										.addComponent(lblNewLabel)
										.addComponent(lblNewLabel_2, Alignment.TRAILING)
										.addComponent(lblNewLabel_3, Alignment.TRAILING))
									.addGap(96)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(nameTextField, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
										.addComponent(loginTextField, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
										.addComponent(eMailTextField, GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblNewLabel_4)
									.addGap(18)
									.addComponent(phoneNumberTextField, GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)))
							.addGap(245)))
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(194)
					.addComponent(lblNewLabel_5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(246))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(21)
							.addComponent(lblNewLabel_5, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewLabel_1)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
									.addComponent(lblNewLabel)
									.addComponent(loginTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addGap(30)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_2)
								.addComponent(nameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(27)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_3)
								.addComponent(eMailTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(35)
									.addComponent(lblNewLabel_4)
									.addGap(49)
									.addComponent(changPasswordBtn))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(27)
									.addComponent(phoneNumberTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
								.addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)))
						.addComponent(removeBtn, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
					.addGap(34))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
