package edu.app.iso.gui;



import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;



import edu.app.iso.persistence.Administrator;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Expert;
import edu.app.iso.persistence.User;
import edu.app.iso.session.Session;
import edu.app.iso.delegate.*;


public class AuthenticationFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPasswordField passwordField;
	private JTextField LoginTextField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AuthenticationFrame frame = new AuthenticationFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AuthenticationFrame() {
		setTitle("Home page ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 891, 697);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setToolTipText("");
		setJMenuBar(menuBar);
		
		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);
		
		JMenuItem mntmHelp = new JMenuItem("Help");
		mntmHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				HelpFrame frame = new HelpFrame();
				frame.setVisible(true);
			}
		});
		mnOptions.add(mntmHelp);
		
		JMenuItem mntmForgotYouPassword = new JMenuItem("Forgot your password?");
		mntmForgotYouPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(LoginTextField.getText()== null)
					 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"Please enter your login !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				else{
					User user= EmployeeServiceDelegate.findEmployee(LoginTextField.getText());
					if (user==null)
					user=AdminServiceDelegate.findAdmin(LoginTextField.getText());
					if (user==null)
						user=ExpertServiceDelegate.findExpert(LoginTextField.getText());
					if (user== null)  JOptionPane.showMessageDialog(new javax.swing.JDialog(),"Inexistant login !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
						
					else {
						EmailSenderDelegate.sendEMail("isoplatform@gmail.com",user.getEmail(),"your iso platform passowrd is"+user.getPassword());
						JOptionPane.showMessageDialog(new javax.swing.JDialog(),"check your e-mail to see your password!", "",JOptionPane.INFORMATION_MESSAGE);
						
					}
					
				}
			}
		});
		mnOptions.add(mntmForgotYouPassword);
		
		JMenuItem mntmQuit = new JMenuItem("Quit");
		mntmQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AuthenticationFrame.this.dispose();
			}
		});
		mnOptions.add(mntmQuit);
		contentPane = new JPanel();
		contentPane.setForeground(Color.WHITE);
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		JLabel label = new JLabel("");
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Raoua\\Desktop\\iSO9001.png"));
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Raoua\\Desktop\\pLATFORM OF QUALITY mANEMENT.png"));
		
		JButton CommitBtn = new JButton("");
		CommitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				
				if (LoginTextField.getText()==null) 
					  JOptionPane.showMessageDialog(new javax.swing.JDialog(),"You haven't entered a login !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				                             
					else {   if (passwordField.getPassword()==null)
						      JOptionPane.showMessageDialog(new javax.swing.JDialog(),"You haven't entered a password !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
					else authenticateUser();
					     
					}
				}

			@SuppressWarnings("deprecation")
			private void authenticateUser() {
				User user=EmployeeServiceDelegate.authenticateEmployee(LoginTextField.getText(), passwordField.getText());
				if(user!=null){
					 AuthenticationFrame.this.dispose();
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									Employee employee=EmployeeServiceDelegate.findEmployee(LoginTextField.getText());
									Session.getInstance().setParametre("AUTH_USER", employee);
									EmployeeFrame frame = new EmployeeFrame();
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
						
					
					
					
					
					
				}
				else {user=ExpertServiceDelegate.authenticateExpert(LoginTextField.getText(), passwordField.getText());
				        if(user!=null){
				        AuthenticationFrame.this.dispose();
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									Expert expert=ExpertServiceDelegate.findExpert(LoginTextField.getText());
									Session.getInstance().setParametre("AUTH_USER", expert);
									ExpertFrame frame = new ExpertFrame();
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
						
				        	
				        	
				        	
				        	
				        	
				        	
				        }
				       else {user=AdminServiceDelegate.authenticateAdmin(LoginTextField.getText(), passwordField.getText());
				       if (user!=null)
				    	   {AuthenticationFrame.this.dispose();
						    EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									Administrator admin=AdminServiceDelegate.findAdmin(LoginTextField.getText());
									
									Session.getInstance().setParametre("AUTH_USER", admin);
									AdminFrame frame = new AdminFrame();
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
						
						
				       }
				       else {JOptionPane.showMessageDialog(new javax.swing.JDialog(),"invalid login and/or password !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				           LoginTextField.setText("");
				           passwordField.setText("");
				       
				       }
				       }
					
				}
					
					
				
			}     
			                                           });
		CommitBtn.setSelectedIcon(new ImageIcon(AuthenticationFrame.class.getResource("/edu/app/iso/icone/Authentification.png")));
		CommitBtn.setForeground(Color.WHITE);
		CommitBtn.setBackground(Color.WHITE);
		CommitBtn.setIcon(new ImageIcon(AuthenticationFrame.class.getResource("/edu/app/iso/icone/Authentification.png")));
		
		JLabel label_1 = new JLabel("New label");
		label_1.setIcon(new ImageIcon(AuthenticationFrame.class.getResource("/edu/app/iso/icone/iSO9001.png")));
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(AuthenticationFrame.class.getResource("/edu/app/iso/icone/attachment.png")));
		
		passwordField = new JPasswordField();
		
		LoginTextField = new JTextField();
		LoginTextField.setColumns(10);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(64)
							.addComponent(label)
							.addGap(45)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(lblPassword)
										.addComponent(lblLogin, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE))
									.addGap(18)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
										.addComponent(passwordField)
										.addComponent(LoginTextField, GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)))
								.addComponent(CommitBtn, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE))
							.addGap(10)
							.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 462, Short.MAX_VALUE)
							.addGap(49)
							.addComponent(lblNewLabel)
							.addGap(584))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(239)
							.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 568, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(195)
							.addComponent(label_2)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(22)
					.addComponent(lblNewLabel_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label_2)
					.addGap(16)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(133)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblLogin)
								.addComponent(LoginTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(72)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblPassword)
								.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(174)
									.addComponent(label))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(52)
									.addComponent(CommitBtn, GroupLayout.PREFERRED_SIZE, 178, GroupLayout.PREFERRED_SIZE))))
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 492, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 423, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(40, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
	
}
