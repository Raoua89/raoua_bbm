package edu.app.iso.gui;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import edu.app.iso.delegate.EnterpriseServiceDelegate;
import edu.app.iso.persistence.BusinessArea;
import edu.app.iso.persistence.Enterprise;

public class EnterpriseListFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
    private List<Enterprise> enterprises;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EnterpriseListFrame frame = new EnterpriseListFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EnterpriseListFrame() {
		enterprises=EnterpriseServiceDelegate.findAllEnterprises();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel masterPanel = new JPanel();
		masterPanel.setBorder(new TitledBorder(null, "Select an enterprise to see its details", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(masterPanel, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(masterPanel, GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_masterPanel = new GroupLayout(masterPanel);
		gl_masterPanel.setHorizontalGroup(
			gl_masterPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_masterPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_masterPanel.setVerticalGroup(
			gl_masterPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_masterPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		masterPanel.setLayout(gl_masterPanel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Enterprise, List<Enterprise>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, enterprises, table);
		//
		BeanProperty<Enterprise, String> enterpriseBeanProperty = BeanProperty.create("name");
		jTableBinding.addColumnBinding(enterpriseBeanProperty).setColumnName("Name").setEditable(false);
		//
		BeanProperty<Enterprise, String> enterpriseBeanProperty_1 = BeanProperty.create("adress");
		jTableBinding.addColumnBinding(enterpriseBeanProperty_1).setColumnName("Adress");
		//
		BeanProperty<Enterprise, BusinessArea> enterpriseBeanProperty_2 = BeanProperty.create("businessArea");
		jTableBinding.addColumnBinding(enterpriseBeanProperty_2).setColumnName("Business area").setEditable(false);
		//
		
		table.setCellSelectionEnabled(true);
		ListSelectionModel cellSelectionModel = table.getSelectionModel();
		cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {

		String selectedData = null;
		int selectedRow = table.getSelectedRow ();
		selectedData = (String) table.getValueAt(selectedRow,0);

		
		 System.out.println(selectedData);
		final Enterprise selectedEnterprise=EnterpriseServiceDelegate.findEnterpriseByName(selectedData);
		
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
			new EnterpriseDetailsFrame(selectedEnterprise).setVisible(true);
			}
			});	
		




		}
		});


		
		
		jTableBinding.setEditable(false);
		jTableBinding.bind();
	}
}
