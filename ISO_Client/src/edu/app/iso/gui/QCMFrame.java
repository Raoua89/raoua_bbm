package edu.app.iso.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.ProposalServiceDelegate;
import edu.app.iso.delegate.QcmServiceDelegate;
import edu.app.iso.delegate.QuestionServiceDelegate;
import edu.app.iso.delegate.ResponseServiceDelegate;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Proposal;
import edu.app.iso.persistence.QCM;
import edu.app.iso.persistence.Question;
import edu.app.iso.persistence.Response;
import edu.app.iso.session.Session;

public class QCMFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField question;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public QCMFrame(final QCM qcm, final int range) {
		 final Employee employee=(Employee) Session.getInstance().getParametre("AUTH_USER");
		 setTitle("Quiz:"+ employee.getName());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 558, 311);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 424, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE))
		);
		
	
		JButton btnQuit = new JButton("Quit");
		btnQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int response = JOptionPane.showConfirmDialog(new javax.swing.JDialog(),"do you really want to quit before completing the quiz ? !!!", "warning",JOptionPane.YES_NO_CANCEL_OPTION);
				 if (response == JOptionPane.YES_OPTION) {
					 QcmServiceDelegate.calculateScore(qcm);
					 QcmServiceDelegate.updateQcm(qcm);
					 QCMFrame.this.dispose();  
					    EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									EmployeeFrame frame = new EmployeeFrame();
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
					   
					 
				 }
					else  {if (response == JOptionPane.NO_OPTION)
						   {
						    QcmServiceDelegate.removeQcm(qcm);
						    QCMFrame.this.dispose();  
						    EventQueue.invokeLater(new Runnable() {
								public void run() {
									try {
										EmployeeFrame frame = new EmployeeFrame();
										frame.setVisible(true);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							});
						   
						   }
						}
					
				 
				
			}
		});
		
		question = new JTextField();
		question.setColumns(10);
		final Question q=QcmServiceDelegate.fetchQuestion(qcm,range);
		List<Proposal> proposals=ProposalServiceDelegate.findProposals(q);
		
		final JCheckBox chckbxProposal1 = new JCheckBox("Proposal 1");
		final JCheckBox chckbxProposal2 = new JCheckBox("Proposal 2");
		final JCheckBox chckbxProposal3 = new JCheckBox("Proposal 3");
		final JCheckBox chckbxProposal4 = new JCheckBox("Proposal 4");
		
		chckbxProposal1.setText(proposals.get(1).getProposalText());
		chckbxProposal2.setText(proposals.get(2).getProposalText());
		chckbxProposal3.setText(proposals.get(3).getProposalText());
		chckbxProposal4.setText(proposals.get(4).getProposalText());
		question.setText(q.getQuestionText());
		question.setEditable(false);
		
		
		
		Response response=QuestionServiceDelegate.fetchResponse(q, qcm);
		if(response!=null){
			chckbxProposal1.setSelected(response.getValue1());
			chckbxProposal2.setSelected(response.getValue2());
			chckbxProposal3.setSelected(response.getValue3());
			chckbxProposal4.setSelected(response.getValue4());
		}
		
		
		
		JButton btnNext = new JButton("next>>");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
            	Response res= new Response(q);
            	int mark=evaluate(res);
            	qcm.getScores().get(range).setValue(mark);
            	
            	
				
				
				QCMFrame.this.dispose();
				if(range<QCM.getNbQuestions())
				 {EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								QCMFrame frame = new QCMFrame(qcm,range+1);
								frame.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				 }
				else 
				{   QCMFrame.this.dispose();
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								EndOfQcmFrame frame = new EndOfQcmFrame(qcm);
								frame.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				 
				}
				
			}

			private int evaluate(Response res) {
				int value=0;
				res.setValue1(chckbxProposal1.isSelected());
				if(res.getQuestion().getProposals().get(1).getProposalState().equals(chckbxProposal1.isSelected()))
					value++;
				res.setValue2(chckbxProposal1.isSelected());
				if(res.getQuestion().getProposals().get(2).getResponseState().equals(chckbxProposal2.isSelected()))
					value++;
				res.setValue3(chckbxProposal1.isSelected());
				if(res.getQuestion().getProposals().get(3).getProposalState().equals(chckbxProposal3.isSelected()))
					value++;
				res.setValue4(chckbxProposal1.isSelected());
				if(res.getQuestion().getProposals().get(4).getProposalState().equals(chckbxProposal4.isSelected()))
					value++;
				
				ResponseServiceDelegate.updateResponse(res);

				return value;
			}
		});
		
		JButton btnNewButton = new JButton("<< back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				QCMFrame.this.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							QCMFrame frame = new QCMFrame(qcm,range-1);
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
				
			}
		});
		
		
		
		
		 
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(40)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btnQuit)
							.addPreferredGap(ComponentPlacement.RELATED, 116, Short.MAX_VALUE)
							.addComponent(btnNewButton)
							.addGap(67)
							.addComponent(btnNext)
							.addGap(60))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(question, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)
							.addGap(65)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(chckbxProposal1)
								.addComponent(chckbxProposal2)
								.addComponent(chckbxProposal3)
								.addComponent(chckbxProposal4))
							.addGap(143))))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addGap(32)
					.addComponent(question, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(chckbxProposal1)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(chckbxProposal2)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(chckbxProposal3)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(chckbxProposal4)
					.addPreferredGap(ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNext)
						.addComponent(btnNewButton)
						.addComponent(btnQuit))
					.addGap(39))
		);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);
	}
}
