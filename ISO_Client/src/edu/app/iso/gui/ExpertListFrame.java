package edu.app.iso.gui;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import edu.app.iso.delegate.ExpertServiceDelegate;
import edu.app.iso.persistence.Administrator;
import edu.app.iso.persistence.Expert;

public class ExpertListFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private List<Expert> experts;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ExpertListFrame frame = new ExpertListFrame(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ExpertListFrame(Administrator admin) {
		
		experts=ExpertServiceDelegate.findAllExperts();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel masterPanel = new JPanel();
		masterPanel.setBorder(new TitledBorder(null, "Select an expert to see its details", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(masterPanel, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(masterPanel, GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_masterPanel = new GroupLayout(masterPanel);
		gl_masterPanel.setHorizontalGroup(
			gl_masterPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_masterPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_masterPanel.setVerticalGroup(
			gl_masterPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_masterPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		masterPanel.setLayout(gl_masterPanel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Expert, List<Expert>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, experts, table);
		//
		BeanProperty<Expert, String> expertBeanProperty = BeanProperty.create("login");
		jTableBinding.addColumnBinding(expertBeanProperty).setColumnName("Login").setEditable(false);
		//
		BeanProperty<Expert, String> expertBeanProperty_1 = BeanProperty.create("name");
		jTableBinding.addColumnBinding(expertBeanProperty_1).setColumnName("Name").setEditable(false);
		//
		
		table.setCellSelectionEnabled(true);
		ListSelectionModel cellSelectionModel = table.getSelectionModel();
		cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {

		String selectedData = null;
		int selectedRow = table.getSelectedRow ();
		selectedData = (String) table.getValueAt(selectedRow,0);

		
		
		final Expert selectedExpert=ExpertServiceDelegate.findExpert(selectedData);
		 System.out.println(selectedExpert);
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
			new ExpertDetailsFrame(selectedExpert).setVisible(true);
			}
			});	
		




		}
		});


		
		
		
		jTableBinding.setEditable(false);
		jTableBinding.bind();
	}
}
