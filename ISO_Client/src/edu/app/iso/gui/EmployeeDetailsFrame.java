package edu.app.iso.gui;


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.EmployeeServiceDelegate;
import edu.app.iso.delegate.EnterpriseServiceDelegate;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Enterprise;

public class EmployeeDetailsFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField specialityTextField;
	private JTextField eMailTextField;
	private JTextField phoneNumberTextField;
	private JTextField loginTextField;
	private JTextField nameTextField;
	private List<Enterprise> enterprises;


	/**
	 * Create the frame.
	 * @param found 
	 */
	public EmployeeDetailsFrame(final Employee employee) {
		
		setTitle("Employees management");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 653, 678);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("Enterprise");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_1 = new JLabel("Speciality");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_2 = new JLabel("Email");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_3 = new JLabel("Phone number");
		lblNewLabel_3.setBackground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_4 = new JLabel("Login");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		loginTextField = new JTextField();
		loginTextField.setColumns(10);
		loginTextField.setText(employee.getLogin());
		
		nameTextField = new JTextField();
		nameTextField.setColumns(10);
		nameTextField.setText(employee.getName());
		
		
		specialityTextField = new JTextField();
		specialityTextField.setColumns(10);
		specialityTextField.setText(employee.getSpeciality());
		
		eMailTextField = new JTextField();
		eMailTextField.setColumns(10);
		eMailTextField.setText(employee.getEmail());
		
		phoneNumberTextField = new JTextField();
		phoneNumberTextField.setColumns(10);
		phoneNumberTextField.setText(Integer.toString(employee.getPhoneNumber()));
		
		
		final JComboBox<String> comboBox = new JComboBox<String>();
		enterprises=EnterpriseServiceDelegate.findAllEnterprises();
		for(Enterprise ent: enterprises)
			comboBox.addItem(ent.getName()) ;
		
		
		JButton backBtn = new JButton("");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EmployeeDetailsFrame.this.dispose();
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new AdminFrame().setVisible(true);
					}
					});
			}
		});
		backBtn.setIcon(new ImageIcon(EmployeeDetailsFrame.class.getResource("/edu/app/iso/icone/Back00.jpg")));
		
		JButton deleteBtn = new JButton("");
		deleteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				 JOptionPane.showConfirmDialog(new javax.swing.JDialog(),"do you really want to delete this employee ? !!!", "warning",JOptionPane.OK_CANCEL_OPTION);
				EmployeeServiceDelegate.removeEmployee(employee);
				 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"account deleted", "",JOptionPane.INFORMATION_MESSAGE);
				 EmployeeDetailsFrame.this.dispose();
			}
		});
		deleteBtn.setIcon(new ImageIcon(EmployeeDetailsFrame.class.getResource("/edu/app/iso/icone/Delet.png")));
		
		JButton updateBtn = new JButton("");
		updateBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Enterprise ent = EnterpriseServiceDelegate.findEnterpriseByName(comboBox.getSelectedItem().toString());
				Employee toUpdate=new Employee(employee.getLogin(),employee.getPassword(),nameTextField.getText(),eMailTextField.getText(),
						Integer.parseInt(phoneNumberTextField.getText()),0,specialityTextField.getText(),ent);
				EmployeeServiceDelegate.updateEmployee(toUpdate);
				
			}
		});
		updateBtn.setIcon(new ImageIcon(EmployeeDetailsFrame.class.getResource("/edu/app/iso/icone/EDIT\u00E0a.jpg")));
		
		
		JLabel lblNewLabel_5 = new JLabel("Name");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_6 = new JLabel("");
		lblNewLabel_6.setIcon(new ImageIcon(EmployeeDetailsFrame.class.getResource("/edu/app/iso/icone/Employee Detail.png")));
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(115)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_4)
						.addComponent(lblNewLabel_5)
						.addComponent(lblNewLabel_1)
						.addComponent(lblNewLabel_2)
						.addComponent(lblNewLabel_3))
					.addGap(48)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(comboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(phoneNumberTextField)
						.addComponent(eMailTextField)
						.addComponent(specialityTextField)
						.addComponent(loginTextField)
						.addComponent(nameTextField, GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE))
					.addContainerGap(141, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(187, Short.MAX_VALUE)
					.addComponent(lblNewLabel_6, GroupLayout.PREFERRED_SIZE, 311, GroupLayout.PREFERRED_SIZE)
					.addGap(129))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(24)
					.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 163, Short.MAX_VALUE)
					.addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
					.addGap(159)
					.addComponent(deleteBtn, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap(143, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_4)
								.addComponent(loginTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(35)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(nameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_5)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(32)
							.addComponent(lblNewLabel_6, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
							.addGap(86)))
					.addGap(37)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(26)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(specialityTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(33)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_2)
						.addComponent(eMailTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(35)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_3)
						.addComponent(phoneNumberTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(66)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
							.addComponent(deleteBtn, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
							.addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)))
					.addGap(106))
		);
		contentPane.setLayout(gl_contentPane);
		
	}
	
}
