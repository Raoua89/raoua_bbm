package edu.app.iso.gui;


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.EmployeeServiceDelegate;
import edu.app.iso.delegate.EnterpriseServiceDelegate;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Enterprise;

public class CreateEmployeeFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField loginTextField;
	private JTextField nameTextField;
	private JTextField specialityTextField;
	private JTextField eMailTextField;
	private JTextField phoneNumberTextField;
	private JPasswordField passwordField;
	private Collection<Enterprise> enterprises;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateEmployeeFrame frame = new CreateEmployeeFrame(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateEmployeeFrame(Enterprise enterprise) {
		
		
		setTitle("Add Employee");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 656, 698);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("Login");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_2 = new JLabel("Name");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_3 = new JLabel("Phone number");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_5 = new JLabel("Speciality");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_6 = new JLabel("Email");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		final JComboBox<String> comboBox = new JComboBox<String>();
		enterprises=EnterpriseServiceDelegate.findAllEnterprises();
		for(Enterprise ent: enterprises)
			comboBox.addItem(ent.getName()) ;
		if (enterprise!=null)
		comboBox.setSelectedItem(enterprise.getName());
		
		
		loginTextField = new JTextField();
		loginTextField.setColumns(10);
		
		nameTextField = new JTextField();
		nameTextField.setColumns(10);
		
		specialityTextField = new JTextField();
		specialityTextField.setColumns(10);
	
		eMailTextField = new JTextField();
		eMailTextField.setColumns(10);
		
		phoneNumberTextField = new JTextField();
		phoneNumberTextField.setColumns(10);
		
		passwordField = new JPasswordField();
		
		JButton addBtn = new JButton("");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (loginTextField.getText()==null) 
					  JOptionPane.showMessageDialog(new javax.swing.JDialog(),"You haven't entered a login !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				                             
					else {   if (passwordField.getPassword()==null)
						      JOptionPane.showMessageDialog(new javax.swing.JDialog(),"You haven't entered a password !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
					else{
				

				final Enterprise ent=EnterpriseServiceDelegate.findEnterpriseByName(comboBox.getSelectedItem().toString());
				@SuppressWarnings("deprecation")
				Employee emp=new Employee(loginTextField.getText(),passwordField.getText(), nameTextField.getText(), eMailTextField.getText(),
						Integer.parseInt(phoneNumberTextField.getText()),0,specialityTextField.getText(),ent);
				EmployeeServiceDelegate.createEmployee(emp);
				JOptionPane.showMessageDialog(new javax.swing.JDialog(),"successfull account creation", "",JOptionPane.INFORMATION_MESSAGE);
				CreateEmployeeFrame.this.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							CreateEmployeeFrame frame = new CreateEmployeeFrame(ent);
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}}}
		});
		addBtn.setIcon(new ImageIcon(CreateEmployeeFrame.class.getResource("/edu/app/iso/icone/ADD.jpg")));
		
		JButton backBtn = new JButton("");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CreateEmployeeFrame.this.dispose();
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new AdminFrame().setVisible(true);
					}
					});
			}
		});
		backBtn.setIcon(new ImageIcon(CreateEmployeeFrame.class.getResource("/edu/app/iso/icone/Back00.jpg")));
		
		JLabel lblNewLabel_7 = new JLabel("Enterprise");
		lblNewLabel_7.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setIcon(new ImageIcon(CreateEmployeeFrame.class.getResource("/edu/app/iso/icone/Add Employee.png")));
		
		
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(24)
					.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 409, Short.MAX_VALUE)
					.addComponent(addBtn, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(177)
					.addComponent(lblNewLabel_4, GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
					.addGap(92))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(82)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_1)
						.addComponent(lblNewLabel_2)
						.addComponent(lblNewLabel_5)
						.addComponent(lblNewLabel_7)
						.addComponent(lblNewLabel_6)
						.addComponent(lblNewLabel_3))
					.addGap(52)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(phoneNumberTextField, 192, 192, Short.MAX_VALUE)
						.addComponent(specialityTextField)
						.addComponent(nameTextField)
						.addComponent(loginTextField)
						.addComponent(passwordField)
						.addComponent(eMailTextField, GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
						.addComponent(comboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addContainerGap(186, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(4)
					.addComponent(lblNewLabel_4, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(loginTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(39)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(45)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_2)
						.addComponent(nameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(48)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_5)
						.addComponent(specialityTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(60)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_7)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(56)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(eMailTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_6))
					.addGap(43)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_3)
						.addComponent(phoneNumberTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
						.addComponent(addBtn, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
					.addGap(18))
		);
		contentPane.setLayout(gl_contentPane);
	}

	

}
