package edu.app.iso.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.AdminServiceDelegate;
import edu.app.iso.delegate.EmployeeServiceDelegate;
import edu.app.iso.delegate.ExpertServiceDelegate;
import edu.app.iso.persistence.Administrator;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Expert;
import edu.app.iso.persistence.User;
import edu.app.iso.session.Session;

public class EditPasswordFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPasswordField actualPasswordField;
	private JPasswordField NewPasswordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditPasswordFrame frame = new EditPasswordFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditPasswordFrame() {
		final User user=(Administrator) Session.getInstance().getParametre("AUTH_USER");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		actualPasswordField = new JPasswordField();
		
		NewPasswordField = new JPasswordField();
		
		JButton editPasswordBtn = new JButton("Modify");
		editPasswordBtn.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				if(actualPasswordField.getText().equals(user.getPassword()))
					{user.setPassword(NewPasswordField.getText());
					if(user.getClass().equals(Employee.class))
						EmployeeServiceDelegate.updateEmployee((Employee)user);
					else {
						if(user.getClass().equals(Expert.class))
							ExpertServiceDelegate.updateExpert((Expert)user);
						else AdminServiceDelegate.updateAdmin((Administrator)user);
					}
					 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"password changed!", "",JOptionPane.INFORMATION_MESSAGE);
					 EditPasswordFrame.this.dispose();
					}
				else JOptionPane.showMessageDialog(new javax.swing.JDialog(),"enter your actual password !!!", "ERROR",JOptionPane.ERROR_MESSAGE); 
				
			}
		});
		
		JLabel lblNewLabel = new JLabel("Actual password");
		
		JLabel lblNewLabel_1 = new JLabel("New password");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(36)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_1))
					.addGap(41)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(NewPasswordField)
						.addComponent(actualPasswordField, GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE))
					.addContainerGap(59, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(251, Short.MAX_VALUE)
					.addComponent(editPasswordBtn)
					.addGap(84))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(48)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(actualPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel))
					.addGap(31)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblNewLabel_1)
						.addComponent(NewPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
					.addComponent(editPasswordBtn)
					.addGap(49))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
