package edu.app.iso.gui;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.TimerServiceDelegate;
import edu.app.iso.persistence.Employee;
import edu.app.iso.session.Session;

public class ShowCourseFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowCourseFrame frame = new ShowCourseFrame(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ShowCourseFrame(byte[] file) {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 414, 240);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 394, 218);
		panel.add(scrollPane);
		Employee current= (Employee) Session.getInstance().getParametre("AUTH_USER");
		if(current.getTimeLeft()==0){
			JOptionPane.showMessageDialog(new javax.swing.JDialog()," your time is up !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
			ShowCourseFrame.this.dispose();
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						EmployeeFrame frame = new EmployeeFrame();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
		
		
		
		// afficher le cours et lancer le timer
		

		
	
		TimerServiceDelegate.start(current.getTimeLeft());
		
		
		
	}
	
	
	public void showFile(){
		try {
			File file=new File("\\monfichier.pdf");
	        Desktop.getDesktop().open(file);
        } catch (Exception e) {
	        e.printStackTrace();
        } 
	}
}
