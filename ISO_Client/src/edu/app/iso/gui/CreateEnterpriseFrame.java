package edu.app.iso.gui;


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.EnterpriseServiceDelegate;
import edu.app.iso.persistence.Administrator;
import edu.app.iso.persistence.BusinessArea;
import edu.app.iso.persistence.Enterprise;

public class CreateEnterpriseFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField nameTextField;
	private JTextField adressTextField;
	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateEnterpriseFrame frame = new CreateEnterpriseFrame(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	

	/**
	 * Create the frame.
	 */
	public CreateEnterpriseFrame(final Administrator admin) {
		setTitle("Add Enterprise");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 652, 692);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel_1 = new JLabel("Business Area");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_2 = new JLabel("Name ");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		
		JLabel lblNewLabel_4 = new JLabel("Adress");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_5 = new JLabel("Category");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		
		nameTextField = new JTextField();
		nameTextField.setColumns(10);
		
		
		
		adressTextField = new JTextField();
		adressTextField.setColumns(10);
		
		
		JButton backBtn = new JButton("");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CreateEnterpriseFrame.this.dispose();
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new AdminFrame().setVisible(true);
					}
					});
			}
		});
		backBtn.setIcon(new ImageIcon(CreateEnterpriseFrame.class.getResource("/edu/app/iso/icone/Back00.jpg")));
		final JComboBox<BusinessArea> businessAreaComboBox = new JComboBox<BusinessArea>();

		businessAreaComboBox.setModel(new DefaultComboBoxModel<BusinessArea>(BusinessArea.values()));
		
		final JComboBox<String> categoryComboBox = new JComboBox<String>();
		categoryComboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"limited", "medium", "high"}));
		
		JButton addBtn = new JButton("");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				final Enterprise ent=new Enterprise(nameTextField.getText(),adressTextField.getText(),(BusinessArea)businessAreaComboBox.getSelectedItem(),categoryComboBox.getSelectedItem().toString(),null);
			EnterpriseServiceDelegate.createEnterprise(ent)	;
				CreateEnterpriseFrame.this.dispose();
				 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"successfull account creation", "",JOptionPane.INFORMATION_MESSAGE);
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new CreateEmployeeFrame(ent).setVisible(true);
					}
					});
				
			}
		});
		addBtn.setIcon(new ImageIcon(CreateEnterpriseFrame.class.getResource("/edu/app/iso/icone/ADD.jpg")));
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(CreateEnterpriseFrame.class.getResource("/edu/app/iso/icone/Add Entre.png")));
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_5)
					.addContainerGap(539, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addContainerGap()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
											.addComponent(lblNewLabel_2)
											.addComponent(lblNewLabel_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addComponent(lblNewLabel_4))
									.addPreferredGap(ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
										.addComponent(nameTextField, GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
										.addComponent(businessAreaComboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(categoryComboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(adressTextField, Alignment.TRAILING))
									.addGap(21))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(35)
									.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addBtn, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap(164, Short.MAX_VALUE)
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 366, GroupLayout.PREFERRED_SIZE)))
					.addGap(96))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(33)
					.addComponent(lblNewLabel)
					.addGap(99)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblNewLabel_2)
						.addComponent(nameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(36)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(businessAreaComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(29)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(adressTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_4))
					.addGap(31)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_5)
						.addComponent(categoryComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(69)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
						.addComponent(addBtn, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
					.addGap(68))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
