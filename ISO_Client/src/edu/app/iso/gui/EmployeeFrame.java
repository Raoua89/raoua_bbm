package edu.app.iso.gui;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import edu.app.iso.delegate.CourseServiceDelegate;
import edu.app.iso.delegate.QcmServiceDelegate;
import edu.app.iso.persistence.Course;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.QCM;
import edu.app.iso.session.Session;

public class EmployeeFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JMenuBar menuBar = new JMenuBar();
	
	private JMenuItem mntmAudits = new JMenuItem("my account");
	private JTable table;
	private List<Course> courses;
	private List<QCM> qcms;
	private JTable table_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmployeeFrame frame = new EmployeeFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EmployeeFrame() {
		
		setTitle("Employee Menu");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 801, 742);
	    final Employee current = (Employee) Session.getInstance().getParametre("AUTH_USER");
	   
		setJMenuBar(menuBar);
			
			JMenu mnNewMenu = new JMenu("Options");
			menuBar.add(mnNewMenu);
				mnNewMenu.add(mntmAudits);
				
				JMenu Aide = new JMenu("Help");
				mnNewMenu.add(Aide);
				
				JMenuItem mntmNewMenuItem_1 = new JMenuItem("learning the principles of the ISO 9001 standard");
				Aide.add(mntmNewMenuItem_1);
				
				JMenuItem mntmNewMenuItem_2 = new JMenuItem("having a specific quality manual for his company");
				Aide.add(mntmNewMenuItem_2);
				
				JMenuItem mntmNewMenuItem = new JMenuItem("Auditing the compatibility of a document or of the company\u2019s method of work with the standard.");
				Aide.add(mntmNewMenuItem);
				
				JMenuItem mntmLogOut = new JMenuItem("log out");
				mntmLogOut.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Session.getInstance().setParametre("AUTH_USER", null);
						EmployeeFrame.this.dispose();
					}
				});
				mnNewMenu.add(mntmLogOut);
			
				mntmAudits.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									EmployeeAccountFrame frame = new EmployeeAccountFrame();
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
							
					}
				});
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.window);
		contentPane.add(panel, BorderLayout.CENTER);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.VERTICAL);
		tabbedPane.setBackground(Color.BLACK);
		
		JPanel CoursePanel = new JPanel();
		CoursePanel.setForeground(Color.WHITE);
		CoursePanel.setBackground(Color.BLACK);
		tabbedPane.addTab("", new ImageIcon(EmployeeFrame.class.getResource("/edu/app/iso/icone/t\u00E9l\u00E9chargement.jpg")), CoursePanel, null);
		

		
		JLabel lblLearningThePrinciples = new JLabel("learning the principles of the ISO 9001 standard");
		lblLearningThePrinciples.setBackground(SystemColor.infoText);
		lblLearningThePrinciples.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		GroupLayout gl_CoursePanel = new GroupLayout(CoursePanel);
		gl_CoursePanel.setHorizontalGroup(
			gl_CoursePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_CoursePanel.createSequentialGroup()
					.addGroup(gl_CoursePanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_CoursePanel.createSequentialGroup()
							.addGap(132)
							.addComponent(lblLearningThePrinciples))
						.addGroup(gl_CoursePanel.createSequentialGroup()
							.addContainerGap()
							.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 750, Short.MAX_VALUE)))
					.addContainerGap())
		);
		gl_CoursePanel.setVerticalGroup(
			gl_CoursePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_CoursePanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 461, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLearningThePrinciples)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_2.setLayout(null);
		
		table = new JTable();
		table.setBorder(new TitledBorder(null, "Select a course to visualize:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		table.setBounds(131, 164, 459, 76);
		panel_2.add(table);
		CoursePanel.setLayout(gl_CoursePanel);
		tabbedPane.setBackgroundAt(0, Color.WHITE);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 775, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(24, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addComponent(tabbedPane)
		);
		
		JPanel auditPanel = new JPanel();
		auditPanel.setBackground(Color.BLACK);
		tabbedPane.addTab("", new ImageIcon(EmployeeFrame.class.getResource("/edu/app/iso/icone/dESIGN REQUEST.png")), auditPanel, null);
		
		JLabel lblElerning = new JLabel("Auditing the compatibility of a document or of the company\u2019s method of work with the standard.");
		lblElerning.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		GroupLayout gl_auditPanel = new GroupLayout(auditPanel);
		gl_auditPanel.setHorizontalGroup(
			gl_auditPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_auditPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 749, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblElerning, GroupLayout.PREFERRED_SIZE, 759, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_auditPanel.setVerticalGroup(
			gl_auditPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_auditPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_auditPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblElerning)
						.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 488, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(15, Short.MAX_VALUE))
		);
		auditPanel.setLayout(gl_auditPanel);
		
		JPanel designPanel = new JPanel();
		designPanel.setForeground(Color.WHITE);
		designPanel.setBackground(Color.BLACK);
		tabbedPane.addTab("", new ImageIcon(EmployeeFrame.class.getResource("/edu/app/iso/icone/Auditts.png")), designPanel, null);
		tabbedPane.setBackgroundAt(2, Color.BLACK);
		
		JLabel lblHavingASpecific = new JLabel("having a specific quality manual for his company");
		lblHavingASpecific.setFont(new Font("Tahoma", Font.BOLD, 24));
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.WHITE);
		GroupLayout gl_designPanel = new GroupLayout(designPanel);
		gl_designPanel.setHorizontalGroup(
			gl_designPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_designPanel.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 750, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblHavingASpecific, GroupLayout.PREFERRED_SIZE, 698, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_designPanel.setVerticalGroup(
			gl_designPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_designPanel.createSequentialGroup()
					.addGroup(gl_designPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_designPanel.createSequentialGroup()
							.addGap(23)
							.addComponent(lblHavingASpecific))
						.addGroup(gl_designPanel.createSequentialGroup()
							.addContainerGap()
							.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 463, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(40, Short.MAX_VALUE))
		);
		designPanel.setLayout(gl_designPanel);
		
		JPanel evaluationPanel = new JPanel();
		evaluationPanel.setBackground(Color.BLACK);
		tabbedPane.addTab("", new ImageIcon(EmployeeFrame.class.getResource("/edu/app/iso/icone/url.jpg")), evaluationPanel, null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(SystemColor.controlLtHighlight);
		GroupLayout gl_evaluationPanel = new GroupLayout(evaluationPanel);
		gl_evaluationPanel.setHorizontalGroup(
			gl_evaluationPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_evaluationPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 750, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_evaluationPanel.setVerticalGroup(
			gl_evaluationPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_evaluationPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		
		JButton viewScoreBtn = new JButton("View my scores");
		viewScoreBtn.setBounds(42, 190, 105, 23);
		viewScoreBtn.addActionListener(new ActionListener() {
			
				
				public void actionPerformed(ActionEvent arg0) {
				table_1.setEnabled(true);	
				qcms=QcmServiceDelegate.findByEmployee(current);
				initDataBinding2();
				
				
				
			}
		});
		
		final JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(296, 125, 213, 20);
		comboBox.setBackground(Color.LIGHT_GRAY);
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Difficulty Level:", "Easy", "Medium", "Hard"}));
		comboBox.setEditable(true);
		
		
		JButton startQcmBtn = new JButton("Start new Quiz");
		startQcmBtn.setBounds(42, 124, 105, 23);
		startQcmBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			  if(comboBox.getSelectedItem().toString().equals("Difficulty Level:"))
				  JOptionPane.showMessageDialog(new javax.swing.JDialog()," Select a difficulty level !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
			  else
			  {   final Employee employee=(Employee) Session.getInstance().getParametre("AUTH_USER");
			            Course course= new Course();
			            CourseServiceDelegate.findByBusinessArea(employee.getEnterprise().getBusinessArea());
				 final QCM qcm = QcmServiceDelegate.prepareQcm(comboBox.getSelectedItem().toString(),employee,course, null);
				 
				 EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								QCMFrame frame = new QCMFrame(qcm,1);
								frame.setVisible(true);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				 
				 
			  }
				
				
			}
		});
		panel_1.setLayout(null);
		
		table_1 = new JTable();
		table_1.setEnabled(false);
		table_1.setBounds(272, 302, 1, 1);
		panel_1.add(table_1);
		panel_1.add(viewScoreBtn);
		panel_1.add(startQcmBtn);
		panel_1.add(comboBox);
		evaluationPanel.setLayout(gl_evaluationPanel);
		panel.setLayout(gl_panel);
		courses= CourseServiceDelegate.findByBusinessArea(current.getEnterprise().getBusinessArea());
		initDataBinding1();
		
	}
	protected void initDataBinding1() {
		JTableBinding<Course, List<Course>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, courses, table);
		//
		BeanProperty<Course, String> courseBeanProperty = BeanProperty.create("title");
		jTableBinding.addColumnBinding(courseBeanProperty).setColumnName("Available Courses:").setEditable(false);
		//
		
	
		table.setCellSelectionEnabled(true);
		ListSelectionModel cellSelectionModel = table.getSelectionModel();
		cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {
    	int selectedRow = table.getSelectedRow ();
		final String selectedData = (String) table.getValueAt(selectedRow,0);
		byte[] file=CourseServiceDelegate.findByTitle(selectedData).getData();
		 
		try {
			FileOutputStream fileOuputStream = new FileOutputStream("temporary.pdf");
			fileOuputStream.write(file);
			fileOuputStream.close();
		} catch (FileNotFoundException e1) {
			
			e1.printStackTrace();
		} catch (IOException e2) {
		
			e2.printStackTrace();
		} 

	    System.out.println("Done");
		
		SwingController controller = new SwingController(); 
		SwingViewBuilder factory = new SwingViewBuilder(controller); 
		JPanel viewerComponentPanel = factory.buildViewerPanel(); 
		
		
		EmployeeFrame.this.getContentPane().add(viewerComponentPanel);
		
		 controller.openDocument("temporary.pdf");
		
		
//		
//        java.awt.EventQueue.invokeLater(new Runnable() {
//			public void run() {
//			new ShowCourseFrame(CourseServiceDelegate.findByTitle(selectedData).getData()).setVisible(true);
//			}
//			});	
	       }
		});
		
	
		jTableBinding.setEditable(false);
		jTableBinding.bind();
	}
	
	
	protected void initDataBinding2() {
		JTableBinding<QCM, List<QCM>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, qcms, table_1);
		//
		BeanProperty<QCM, Date> qCMBeanProperty = BeanProperty.create("date");
		jTableBinding.addColumnBinding(qCMBeanProperty).setColumnName("Date:").setEditable(false);
		//
		BeanProperty<QCM, String> qCMBeanProperty_1 = BeanProperty.create("difficultyLevel");
		jTableBinding.addColumnBinding(qCMBeanProperty_1).setColumnName("Difficulty Level:").setEditable(false);
		//
		BeanProperty<QCM, Integer> qCMBeanProperty_2 = BeanProperty.create("finalScore");
		jTableBinding.addColumnBinding(qCMBeanProperty_2).setColumnName("Score").setEditable(false);
		//
		jTableBinding.setEditable(false);
		jTableBinding.bind();
	}
}
