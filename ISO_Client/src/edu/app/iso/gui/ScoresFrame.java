package edu.app.iso.gui;

import java.awt.EventQueue;
import java.util.Date;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import edu.app.iso.delegate.QcmServiceDelegate;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.QCM;
import edu.app.iso.session.Session;

public class ScoresFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private List <QCM> qcms;
	private JPanel panel;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ScoresFrame frame = new ScoresFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ScoresFrame() {
		Employee employee= (Employee) Session.getInstance().getParametre("AUTH_USER");
		qcms= QcmServiceDelegate.findByEmployee(employee);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "my quiz scores", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 414, 240);
		contentPane.add(panel);
		
		table = new JTable();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(159)
					.addComponent(table, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(254, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(145, Short.MAX_VALUE)
					.addComponent(table, GroupLayout.PREFERRED_SIZE, 1, GroupLayout.PREFERRED_SIZE)
					.addGap(94))
		);
		panel.setLayout(gl_panel);
		initDataBindings();
		
	}
	protected void initDataBindings() {
		JTableBinding<QCM, List<QCM>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, qcms, table);
		//
		BeanProperty<QCM, Date> qCMBeanProperty = BeanProperty.create("date");
		jTableBinding.addColumnBinding(qCMBeanProperty).setColumnName("date").setEditable(false);
		//
		BeanProperty<QCM, Integer> qCMBeanProperty_1 = BeanProperty.create("finalScore");
		jTableBinding.addColumnBinding(qCMBeanProperty_1).setColumnName("score").setEditable(false);
		//
		jTableBinding.setEditable(false);
		jTableBinding.bind();
	}
}
