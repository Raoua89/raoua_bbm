package edu.app.iso.gui;

import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import edu.app.iso.persistence.Course;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CoursesListFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private List<Course> courses;
	private JTable table;
	
	
	

	/**
	 * Create the frame.
	 */
	public CoursesListFrame(List<Course> list) {
		setTitle("Courses management");
		courses= list;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new TitledBorder(null, "Courses list", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		scrollPane.setBounds(10, 11, 414, 112);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton addQuestionBtn = new JButton("add question");
		addQuestionBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		addQuestionBtn.setBounds(20, 144, 123, 23);
		contentPane.add(addQuestionBtn);
		
		JButton editCourseBtn = new JButton("edit");
		editCourseBtn.setBounds(20, 178, 123, 23);
		contentPane.add(editCourseBtn);
		
		JButton removeCourseBtn = new JButton("remove");
		removeCourseBtn.setBounds(20, 212, 123, 23);
		contentPane.add(removeCourseBtn);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Course, List<Course>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, courses, table);
		//
		BeanProperty<Course, Integer> courseBeanProperty = BeanProperty.create("courseId");
		jTableBinding.addColumnBinding(courseBeanProperty).setColumnName("course ID").setEditable(false);
		//
		BeanProperty<Course, String> courseBeanProperty_1 = BeanProperty.create("title");
		jTableBinding.addColumnBinding(courseBeanProperty_1).setColumnName("Title").setEditable(false);
		//
		jTableBinding.setEditable(false);
		jTableBinding.bind();
	}
}
