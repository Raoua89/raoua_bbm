package edu.app.iso.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.SoftBevelBorder;

import edu.app.iso.delegate.QuestionServiceDelegate;
import edu.app.iso.persistence.Course;
import edu.app.iso.persistence.Proposal;
import edu.app.iso.persistence.Question;

public class ManageQuestionFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField questionTxtField;
	private JTextField prop1TxtField;
	private JTextField prop2TxtField;
	private JTextField prop3TxtField;
	private JTextField prop4TxtField;
	private JLabel lblQuestionText;
	private JButton addBtn;
	private  JCheckBox proposal1;
	private  JCheckBox proposal2;
	private  JCheckBox proposal3;
	private  JCheckBox proposal4;
	private JComboBox<String> comboBox;

	

	/**
	 * Create the frame.
	 */
	public ManageQuestionFrame(final Course course, final Question question) {
		setTitle("Courses management");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.BLACK, Color.LIGHT_GRAY, Color.BLACK, Color.LIGHT_GRAY));
		panel.setBounds(10, 11, 414, 240);
		contentPane.add(panel);
		panel.setLayout(null);
		
		questionTxtField = new JTextField();
		questionTxtField.setBounds(10, 35, 172, 20);
		panel.add(questionTxtField);
		questionTxtField.setColumns(10);
		
		proposal1 = new JCheckBox("Proposal 1:");
		proposal1.setBounds(10, 87, 97, 23);
		panel.add(proposal1);
		
		proposal2 = new JCheckBox("Proposal 2 :");
		proposal2.setBounds(10, 113, 97, 23);
		panel.add(proposal2);
		
		proposal3 = new JCheckBox("Proposal 3 :");
		proposal3.setBounds(10, 144, 97, 23);
		panel.add(proposal3);
		
		proposal4 = new JCheckBox("Proposal 4 :");
		proposal4.setBounds(10, 170, 97, 23);
		panel.add(proposal4);
		
		prop1TxtField = new JTextField();
		prop1TxtField.setBounds(141, 87, 263, 20);
		panel.add(prop1TxtField);
		prop1TxtField.setColumns(10);
		
		prop2TxtField = new JTextField();
		prop2TxtField.setBounds(141, 116, 263, 20);
		panel.add(prop2TxtField);
		prop2TxtField.setColumns(10);
		
		prop3TxtField = new JTextField();
		prop3TxtField.setBounds(141, 142, 263, 20);
		panel.add(prop3TxtField);
		prop3TxtField.setColumns(10);
		
		prop4TxtField = new JTextField();
		prop4TxtField.setBounds(141, 173, 263, 20);
		panel.add(prop4TxtField);
		prop4TxtField.setColumns(10);
		
		lblQuestionText = new JLabel("Question Text:");
		lblQuestionText.setBounds(10, 11, 97, 14);
		
		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Difficulty Level :", "Easy", "Medium", "Hard"}));
		comboBox.setBounds(232, 35, 143, 20);
		panel.add(comboBox);
		panel.add(lblQuestionText);
		
     	initFrame(question);
		
		
		
		addBtn = new JButton("save");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selectedDif= comboBox.getSelectedItem().toString();
				if(selectedDif.equals("Difficulty Level:"))
					 JOptionPane.showMessageDialog(new javax.swing.JDialog()," Select a difficulty level !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				else {
					 if(verifyInputs(questionTxtField,prop1TxtField,prop2TxtField,prop3TxtField,prop4TxtField)){
							
						Question quest= new Question(questionTxtField.getText(),selectedDif,course);
						if (quest!=null)
							System.out.println("create");
						Proposal prop1 = new Proposal(prop1TxtField.getText(),proposal1.isSelected(),quest,null);
						Proposal prop2 = new Proposal(prop1TxtField.getText(),proposal2.isSelected(),quest,null);
						Proposal prop3 = new Proposal(prop1TxtField.getText(),proposal3.isSelected(),quest,null);
						Proposal prop4 = new Proposal(prop1TxtField.getText(),proposal4.isSelected(),quest,null);
				
						quest.addProposal(prop1);
						quest.addProposal(prop2);
						quest.addProposal(prop3);
						quest.addProposal(prop4);
						quest.setScores(null);
						quest.setResponses(null);
						if(question==null){
						
					        QuestionServiceDelegate.createQuestion(quest);
					        JOptionPane.showMessageDialog(new javax.swing.JDialog(), "question added with success");
						}
						else{
							System.out.println("update");
							quest.setQuestionId(question.getQuestionId());
							QuestionServiceDelegate.updateQuestion(quest);
							 JOptionPane.showMessageDialog(new javax.swing.JDialog(), "modifications saved");
						}

								 int response= JOptionPane.showConfirmDialog(new javax.swing.JDialog(),"do you want to add an other question??", "",JOptionPane.YES_NO_OPTION);
								
								if(response == JOptionPane.NO_OPTION){
									ManageQuestionFrame.this.dispose();
									EventQueue.invokeLater(new Runnable() {
										public void run() {
											try {
												ExpertFrame frame = new ExpertFrame();
												frame.setVisible(true);
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									});
									}
								else{
									ManageQuestionFrame.this.dispose();
									
									EventQueue.invokeLater(new Runnable() {
										public void run() {
											try {
												ManageQuestionFrame frame = new ManageQuestionFrame(course,null);
												frame.setVisible(true);
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									});
								} 
								 
								
							}
									 else{
										 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"Please verify your input !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
									 }}}

			private boolean verifyInputs(JTextField t1,
					JTextField t2, JTextField t3,
					JTextField t4, JTextField t5) {
				
				return (verifyInput(t1)&&verifyInput(t2)&&verifyInput(t3)&&verifyInput(t4)&&verifyInput(t5));
			}

			private boolean verifyInput(JTextField input) {
				if(input.getText()!=null&& input.getText()!=" ")
				return true;
				return false;
			}
						});
		addBtn.setBounds(315, 206, 89, 23);
		panel.add(addBtn);
	}



	private void initFrame(Question question) {
		if(question!=null){
		questionTxtField.setText(question.getQuestionText());
		comboBox.setSelectedItem(question.getDifficultyLevel());
		 List<Proposal> proposals= question.getProposals();
		 
		 
		 prop1TxtField.setText(proposals.get(1).getProposalText());
		 proposal1.setSelected(proposals.get(1).getProposalState());
		 
		 prop2TxtField.setText(proposals.get(2).getProposalText());
		 proposal2.setSelected(proposals.get(2).getProposalState());
		 
		 prop3TxtField.setText(proposals.get(2).getProposalText());
		 proposal3.setSelected(proposals.get(3).getProposalState());
		 
		 prop4TxtField.setText(proposals.get(4).getProposalText());
		 proposal4.setSelected(proposals.get(4).getProposalState());
		
	}}
}
