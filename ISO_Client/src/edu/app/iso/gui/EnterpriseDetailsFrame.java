package edu.app.iso.gui;


import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.EnterpriseServiceDelegate;
import edu.app.iso.persistence.BusinessArea;
import edu.app.iso.persistence.Enterprise;

public class EnterpriseDetailsFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField idTextField;
	private JTextField nameTextField;
	private JTextField adressTextField;

	

	/**
	 * Create the frame.
	 */
	public EnterpriseDetailsFrame(final Enterprise enterprise) {
		setTitle("Enterprise Details");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 652, 692);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("id");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblNewLabel_1 = new JLabel("Business Area");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_2 = new JLabel("Name ");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_3 = new JLabel("Show the emplyees list ");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		
		JLabel lblNewLabel_4 = new JLabel("Adress");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_5 = new JLabel("Category");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		idTextField = new JTextField();
		idTextField.setColumns(10);
		idTextField.setText(Integer.toString(enterprise.getId()));
		
		nameTextField = new JTextField();
		nameTextField.setColumns(10);
		nameTextField.setText(enterprise.getName());
		
		
		adressTextField = new JTextField();
		adressTextField.setColumns(10);
		adressTextField.setText(enterprise.getAdress());
		
		final JComboBox<BusinessArea> businessAreaComboBox = new JComboBox<BusinessArea>();
		businessAreaComboBox.setModel(new DefaultComboBoxModel<BusinessArea>(BusinessArea.values()));
		businessAreaComboBox.setSelectedItem(enterprise.getBusinessArea());
		
		
		
		
		final JComboBox<String> categoryComboBox = new JComboBox<String>();
		categoryComboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"limited", "medium", "high"}));
		categoryComboBox.setSelectedItem(enterprise.getCategory());
		
		
		JButton deleteBtn = new JButton("");
		deleteBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				 JOptionPane.showConfirmDialog(new javax.swing.JDialog(),"do you really want to delete this enterprise and all its employees ? !!!", "warning",JOptionPane.OK_CANCEL_OPTION);
				EnterpriseServiceDelegate.removeEnterprise(enterprise);
				 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"account deleted", "",JOptionPane.INFORMATION_MESSAGE);
				 EnterpriseDetailsFrame.this.dispose();
			}
		});
		deleteBtn.setIcon(new ImageIcon(EnterpriseDetailsFrame.class.getResource("/edu/app/iso/icone/Delet.png")));
		
		JButton updateBtn = new JButton("");
		updateBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Enterprise ent=new Enterprise(nameTextField.getText(),adressTextField.getText(),(BusinessArea)businessAreaComboBox.getSelectedItem(),categoryComboBox.getSelectedItem().toString(),null);
				ent.setId(enterprise.getId());
				EnterpriseServiceDelegate.updateEnterprise(ent);
				 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"modifications saved!", "",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		updateBtn.setIcon(new ImageIcon(EnterpriseDetailsFrame.class.getResource("/edu/app/iso/icone/EDIT\u00E0a.jpg")));
		
		JButton backBtn = new JButton("");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new AdminFrame().setVisible(true);
					}
					});
			}
		});
		backBtn.setIcon(new ImageIcon(EnterpriseDetailsFrame.class.getResource("/edu/app/iso/icone/Back00.jpg")));
		
		JButton showEmployeesBtn = new JButton("");
		showEmployeesBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new EmployeesListFrame(enterprise).setVisible(true);
					}
					});
			}
		});
		showEmployeesBtn.setIcon(new ImageIcon(EnterpriseDetailsFrame.class.getResource("/edu/app/iso/icone/list.jpg")));
		
		JLabel lblNewLabel_6 = new JLabel("");
		lblNewLabel_6.setIcon(new ImageIcon(EnterpriseDetailsFrame.class.getResource("/edu/app/iso/icone/Enterprisedetail.png")));
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addContainerGap()
									.addComponent(lblNewLabel_3, GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 187, Short.MAX_VALUE)
									.addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)))
							.addGap(142))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel)
								.addComponent(lblNewLabel_2)
								.addComponent(lblNewLabel_1)
								.addComponent(lblNewLabel_4))
							.addGap(65)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(categoryComboBox, Alignment.LEADING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(nameTextField)
								.addComponent(idTextField, Alignment.LEADING)
								.addComponent(businessAreaComboBox, Alignment.LEADING, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(adressTextField, Alignment.LEADING))
							.addPreferredGap(ComponentPlacement.RELATED, 50, Short.MAX_VALUE)))
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(deleteBtn, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(showEmployeesBtn, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)))
					.addGap(18))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_5)
					.addContainerGap(539, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(179)
					.addComponent(lblNewLabel_6, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(198, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_6)
					.addGap(7)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(idTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(38)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblNewLabel_2)
						.addComponent(nameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(36)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(businessAreaComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(26)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(adressTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
									.addGap(45)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel_5)
										.addComponent(categoryComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
									.addComponent(lblNewLabel_3)
									.addGap(55))
								.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(showEmployeesBtn, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
									.addGap(41)))
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(backBtn, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
										.addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
										.addComponent(deleteBtn, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE))))
							.addGap(23))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblNewLabel_4)
							.addContainerGap(299, Short.MAX_VALUE))))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
