package edu.app.iso.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.CourseServiceDelegate;
import edu.app.iso.delegate.QcmServiceDelegate;
import edu.app.iso.persistence.Course;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.QCM;
import edu.app.iso.session.Session;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class EndOfQcmFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;


	

	/**
	 * Create the frame.
	 */
	public EndOfQcmFrame(final QCM qcm) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240, 240, 240));
		panel.setBounds(10, 11, 414, 240);
		contentPane.add(panel);
		panel.setLayout(null);
		final JFormattedTextField scoreTxtField = new JFormattedTextField();
		scoreTxtField.setEnabled(false);
		scoreTxtField.setEditable(false);
		scoreTxtField.setBounds(231, 98, 173, 20);
		panel.add(scoreTxtField);
		
		
		JButton scoreBtn = new JButton("View my score");
		scoreBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				QcmServiceDelegate.calculateScore(qcm);
				scoreTxtField.setText("your score is"+ Integer.toString(qcm.getFinalScore()));
				scoreTxtField.setEnabled(true);
			}
		});
		scoreBtn.setBounds(38, 97, 138, 23);
		panel.add(scoreBtn);
		
		JButton allScoresBtn = new JButton("View all my scores");
		allScoresBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EndOfQcmFrame.this.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							ScoresFrame frame = new ScoresFrame();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			
			}
		});
		allScoresBtn.setBounds(38, 144, 138, 23);
		panel.add(allScoresBtn);
		
		JLabel lblNewLabel = new JLabel("End of the quiz");
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel.setBounds(10, 42, 162, 14);
		panel.add(lblNewLabel);
		
		final JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Difficulty Level:", "Easy", "Medium", "Hard"}));
		comboBox.setEditable(true);
		comboBox.setBounds(231, 191, 173, 20);
		panel.add(comboBox);
		
		JButton newQcmBtn = new JButton("start a new quiz");
		newQcmBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				  if(comboBox.getSelectedItem().equals("Difficulty Level:"))
					  JOptionPane.showMessageDialog(new javax.swing.JDialog()," Select a difficulty level !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				  else
				  {   final Employee employee=(Employee) Session.getInstance().getParametre("AUTH_USER");
				            Course course= new Course();
				            CourseServiceDelegate.findByBusinessArea(employee.getEnterprise().getBusinessArea());
					 final QCM qcm = QcmServiceDelegate.prepareQcm(comboBox.getSelectedItem().toString(),employee,course, null);
					 EndOfQcmFrame.this.dispose();
					 EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									QCMFrame frame = new QCMFrame(qcm,1);
									frame.setVisible(true);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
					 
					 
				  }
					
					
				
				
				
			
			}
		});
		newQcmBtn.setBounds(38, 190, 138, 23);
		panel.add(newQcmBtn);
		
		
	}
}
