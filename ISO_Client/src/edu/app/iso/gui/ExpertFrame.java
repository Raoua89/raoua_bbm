package edu.app.iso.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import edu.app.iso.delegate.CourseServiceDelegate;
import edu.app.iso.delegate.QcmServiceDelegate;
import edu.app.iso.delegate.QuestionServiceDelegate;
import edu.app.iso.persistence.BusinessArea;
import edu.app.iso.persistence.Course;
import edu.app.iso.persistence.Question;
import edu.app.iso.session.Session;
import javax.swing.JScrollPane;

public class ExpertFrame extends JFrame {

	/**
	 * 
	 */
	
	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField titleTextField;
	private JTextField textField_1;
	private List<Course> courses;
	private List<Question> questions;
	private int selectedCourse=0 ;
    private int selectedQuestion=0;
    private String selectedCourseTitle=null;
    private byte[] data= null;
    private JTable table;
    private JTable table_1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ExpertFrame frame = new ExpertFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ExpertFrame() {
		
		questions=null;
		setTitle("Expert Menu");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 944, 662);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("my account");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							ExpertAccountFrame frame = new ExpertAccountFrame();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		});
		mnOptions.add(mntmNewMenuItem);
		
		JMenuItem mntmHelp = new JMenuItem("Help");
		mnOptions.add(mntmHelp);
		
		JMenuItem mntmLoOut = new JMenuItem("log out");
		mntmLoOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Session.getInstance().setParametre("AUTH_USER", null);
				ExpertFrame.this.dispose();
			}
		});
		mnOptions.add(mntmLoOut);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 930, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(95, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 618, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		tabbedPane.addTab("Requests management", null, panel, null);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 925, Short.MAX_VALUE)
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGap(0, 590, Short.MAX_VALUE)
		);
		panel.setLayout(gl_panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		tabbedPane.addTab("Courses management", null, panel_1, null);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Courses", TitledBorder.LEADING, TitledBorder.ABOVE_TOP, null, null));
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Questions", TitledBorder.LEADING, TitledBorder.ABOVE_TOP, null, null));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(24)
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE)
					.addGap(30)
					.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(21, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel_4, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)
						.addComponent(panel_3, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE))
					.addGap(40))
		);
		panel_4.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBounds(46, 99, 86, 20);
		int n=QcmServiceDelegate.getNbQuestion();
		textField_1.setText(Integer.toString(n));
		panel_4.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblUpdateNumberQuestion = new JLabel("update the number of questions per quiz");
		lblUpdateNumberQuestion.setBounds(20, 35, 212, 14);
		panel_4.add(lblUpdateNumberQuestion);
		
		JButton nbQuestionBtn = new JButton("save");
		nbQuestionBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				QcmServiceDelegate.updateNbQuestion(Integer.parseInt(textField_1.getText()));
				JOptionPane.showMessageDialog(new javax.swing.JDialog(),"modification saved!!", "OK!",JOptionPane.INFORMATION_MESSAGE);
			}
		});
		nbQuestionBtn.setBounds(300, 98, 119, 23);
		panel_4.add(nbQuestionBtn);
		
		JButton addQstBtn = new JButton("add new ");
		addQstBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				final Course course=CourseServiceDelegate.findCourse(1);
				ExpertFrame.this.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							
							ManageQuestionFrame frame = new ManageQuestionFrame(course,null);
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		});
		addQstBtn.setBounds(27, 471, 105, 23);
		panel_4.add(addQstBtn);
		
		JButton btnNewButton_4 = new JButton("update");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final Course course=CourseServiceDelegate.findCourse(selectedCourse);
				final Question question=QuestionServiceDelegate.findQuestion(selectedQuestion);
				ExpertFrame.this.dispose();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							
							ManageQuestionFrame frame = new ManageQuestionFrame(course,question);
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		});
		btnNewButton_4.setBounds(188, 471, 89, 23);
		panel_4.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("remove");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(selectedQuestion==0)
					 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"Please select a question !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				else{
		int response =	JOptionPane.showConfirmDialog(new javax.swing.JDialog(),"do you really want to remove this question?", "warning!!",JOptionPane.YES_NO_OPTION);
			if(response==JOptionPane.YES_OPTION)
				{Question question= new Question();
				question.setQuestionId(selectedQuestion);
				QuestionServiceDelegate.removeQuestion(question);
			JOptionPane.showMessageDialog(new javax.swing.JDialog(),"question removed!!", "OK!",JOptionPane.INFORMATION_MESSAGE);
			
			
				}
			}}
				
			
		});
		btnNewButton_5.setBounds(340, 471, 89, 23);
		panel_4.add(btnNewButton_5);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 158, 409, 287);
		panel_4.add(scrollPane);
		
		table_1 = new JTable();
		table_1.setBorder(new TitledBorder(null, "Questions list:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		scrollPane.setViewportView(table_1);
		
		final JComboBox<BusinessArea> comboBox = new JComboBox<BusinessArea>();
		comboBox.setModel(new DefaultComboBoxModel<BusinessArea>(BusinessArea.values()));
		comboBox.setBounds(16, 112, 230, 20);
		panel_3.setLayout(null);
		panel_3.add(comboBox);
		
		titleTextField = new JTextField();
		titleTextField.setBounds(16, 47, 230, 20);
		panel_3.add(titleTextField);
		titleTextField.setColumns(10);
		JButton addCourseBtn = new JButton("select a file");
		panel_3.add(addCourseBtn);
		addCourseBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				 
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Specify a file to save");    
				 
				int userSelection = fileChooser.showSaveDialog(ExpertFrame.this);
				 
				if (userSelection == JFileChooser.APPROVE_OPTION) {
				    File fileToSave = fileChooser.getSelectedFile();
				    System.out.println("Save as file: " + fileToSave.getAbsolutePath());
				    data=new byte[((int) fileToSave.length())-1];
				
				try {
					new FileOutputStream(fileToSave).write(data);
				} catch (FileNotFoundException e1) {
					
					e1.printStackTrace();
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}
				if(data!=null)
					System.out.println("ok");
				}
				
					
				}
	}
		);
		addCourseBtn.setBounds(16, 78, 230, 23);
		
		
		JButton btnNewButton = new JButton("update");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(selectedCourse==0)
					 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"Please select a course !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				else{
		int response =	JOptionPane.showConfirmDialog(new javax.swing.JDialog(),"do you really want to update this course??", "warning!!",JOptionPane.YES_NO_OPTION);
			if(response==JOptionPane.YES_OPTION)
				{Course course= new Course();
				course.setCourseId(selectedCourse);
				course.setTitle(selectedCourseTitle);
				CourseServiceDelegate.updateCourse(course);
			JOptionPane.showConfirmDialog(new javax.swing.JDialog(),"modifications saved!!", "OK!",JOptionPane.INFORMATION_MESSAGE);
			
						
				}
			}}
			
		});
		btnNewButton.setBounds(44, 464, 109, 23);
		panel_3.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("remove");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(selectedCourse==0)
					 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"Please select a course !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				else{
		int response =	JOptionPane.showConfirmDialog(new javax.swing.JDialog(),"do you really want to remove this course?", "warning!!",JOptionPane.YES_NO_OPTION);
			if(response==JOptionPane.YES_OPTION)
				{Course course= new Course();
				course.setCourseId(selectedCourse);
				CourseServiceDelegate.removeCourse(course);
			JOptionPane.showConfirmDialog(new javax.swing.JDialog(),"course removed!!", "OK!",JOptionPane.INFORMATION_MESSAGE);
			
			
			
				}
			}}
		});
		btnNewButton_1.setBounds(251, 464, 109, 23);
		panel_3.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("add");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(titleTextField.getText().equals(""))
					 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"Please enter a title for the new course !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
					else{
						
						
						if(data==null)
							 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"Please select a file !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
							else{
					                Course course=new Course();
									course.setData(data);
									course.setTitle(titleTextField.getText());
									course.setBusinessArea((BusinessArea) comboBox.getSelectedItem());
									
								CourseServiceDelegate.createCourse(course);
								JOptionPane.showMessageDialog(new javax.swing.JDialog(),"course added!!", "OK!",JOptionPane.INFORMATION_MESSAGE);
									
							}
							
					}}
		});
		btnNewButton_2.setBounds(269, 46, 121, 23);
		panel_3.add(btnNewButton_2);
		
		JButton btnNewButton_6 = new JButton("list the courses");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				courses = CourseServiceDelegate.findByBusinessArea((BusinessArea) comboBox.getSelectedItem());
				initDataBinding1();
			}
		});
		btnNewButton_6.setBounds(269, 111, 121, 23);
		panel_3.add(btnNewButton_6);
		
		table = new JTable();
		table.setBorder(new TitledBorder(null, "Courses list", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		table.setBounds(16, 178, 374, 32);
		panel_3.add(table);
		panel_1.setLayout(gl_panel_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		tabbedPane.addTab("Audits management", null, panel_2, null);
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGap(0, 925, Short.MAX_VALUE)
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGap(0, 590, Short.MAX_VALUE)
		);
		panel_2.setLayout(gl_panel_2);
		contentPane.setLayout(gl_contentPane);
		initDataBinding1();
		
		

	}
	protected void initDataBinding1() {
		JTableBinding<Course, List<Course>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ_WRITE, courses, table);
		//
		BeanProperty<Course, Integer> courseBeanProperty = BeanProperty.create("courseId");
		jTableBinding.addColumnBinding(courseBeanProperty).setColumnName("Course ID").setEditable(false);
		//
		BeanProperty<Course, String> courseBeanProperty_1 = BeanProperty.create("title");
		jTableBinding.addColumnBinding(courseBeanProperty_1).setColumnName("Title");
		//
		
		
		
		table.setCellSelectionEnabled(true);
		ListSelectionModel cellSelectionModel = table.getSelectionModel();
		cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {

		
		int selectedRow = table.getSelectedRow ();
		selectedCourse = (int) table.getValueAt(selectedRow,0);
        System.out.println(selectedCourse);
        questions=QuestionServiceDelegate.findQuestions(CourseServiceDelegate.findCourse(selectedCourse));
        initDataBindings();
		
	}
		});
		
		
		jTableBinding.bind();
	}
	protected void initDataBindings() {
		JTableBinding<Question, List<Question>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, questions, table_1);
		//
		BeanProperty<Question, Integer> questionBeanProperty = BeanProperty.create("questionId");
		jTableBinding.addColumnBinding(questionBeanProperty).setColumnName("Question ID:");
		//
		BeanProperty<Question, String> questionBeanProperty_1 = BeanProperty.create("questionText");
		jTableBinding.addColumnBinding(questionBeanProperty_1).setColumnName("Question Text:");
		//
		BeanProperty<Question, String> questionBeanProperty_2 = BeanProperty.create("difficultyLevel");
		jTableBinding.addColumnBinding(questionBeanProperty_2).setColumnName("Difficulty Level:");
		//
		
		
		
		table_1.setCellSelectionEnabled(true);
		ListSelectionModel cellSelectionModel = table_1.getSelectionModel();
		cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {

		
		int selectedRow = table_1.getSelectedRow ();
		selectedQuestion = (int) table_1.getValueAt(selectedRow,0);
        System.out.println(selectedQuestion);
        
       
		
	}
		});
		
		
		
		jTableBinding.bind();
	}
		}
