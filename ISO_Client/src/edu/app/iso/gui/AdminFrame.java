package edu.app.iso.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import edu.app.iso.delegate.AdminServiceDelegate;
import edu.app.iso.delegate.EmployeeServiceDelegate;
import edu.app.iso.delegate.EnterpriseServiceDelegate;
import edu.app.iso.delegate.ExpertServiceDelegate;
import edu.app.iso.persistence.Administrator;
import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Enterprise;
import edu.app.iso.persistence.Expert;
import edu.app.iso.session.Session;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class AdminFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField LoginTextFieldAdmin;
	private JTextField LoginTextFieldExp;
	private JTextField LoginTextFieldEmp;
	private JTextField nameTextFieldEnt;
	Administrator admin;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminFrame frame = new AdminFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	
	
	
	/**
	 * Create the frame.
	 */
	public AdminFrame() {
		
		final Administrator admin=(Administrator) Session.getInstance().getParametre("AUTH_USER");
		
		setTitle("Administration Menu ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 766, 679);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);
		
		JMenuItem mntmMyAccount = new JMenuItem("My account");
		mntmMyAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							AdminAccountFrame frame = new AdminAccountFrame();
							frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});	
				
				
			}
		});
		mnOptions.add(mntmMyAccount);
		
		JMenuItem mntmQuit = new JMenuItem("Quit");
		mnOptions.add(mntmQuit);
		
		JMenuItem mntmLogOut = new JMenuItem("log out");
		mntmLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Session.getInstance().setParametre("AUTH_USER", null);
				AdminFrame.this.dispose();
			}
		});
		mnOptions.add(mntmLogOut);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 755, 751);
		
		JPanel AdminPanel = new JPanel();
		AdminPanel.setBackground(Color.WHITE);
		tabbedPane.addTab("Administrators mangement", AdminPanel);
		
		JLabel lblEnterTheAdministrator = new JLabel("Enter the administrator Login ");
		lblEnterTheAdministrator.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		LoginTextFieldAdmin = new JTextField();
		LoginTextFieldAdmin.setColumns(10);
		
		JButton SearchBtnAdmin = new JButton("");
		SearchBtnAdmin.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/Serch.jpg")));
		SearchBtnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(LoginTextFieldAdmin.getText()==null|LoginTextFieldAdmin.getText()==" ")
					JOptionPane.showMessageDialog(new javax.swing.JDialog()," Enter an administrator login !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				else{
					
				 
				final Administrator admin=AdminServiceDelegate.findAdmin(LoginTextFieldAdmin.getText());
				 if(admin==null)
					 JOptionPane.showMessageDialog(new javax.swing.JDialog(),"Inexistant administrator login !!!", "ERROR",JOptionPane.ERROR_MESSAGE); 
				 
				 else {
					 
					AdminFrame.this.dispose();
					 java.awt.EventQueue.invokeLater(new Runnable() {
							public void run() {
							new AdminDetailsFrame(admin).setVisible(true);
							}
							});
				 }
			}
		}});
		
		JButton ListingBtnAdmin = new JButton("Show all");
		ListingBtnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminFrame.this.dispose();
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new AdminListFrame().setVisible(true);
					}
					});
			}
		});
		
		JButton AddBtnAdmin = new JButton("Add new ");
		AddBtnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminFrame.this.dispose();
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new CreateAdminFrame().setVisible(true);
					}
					});	
				
			}
		});
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/Database Administrator (1).jpg")));
		
		JLabel label = new JLabel("");
		
		JLabel lblNewLabel_5 = new JLabel("");
		lblNewLabel_5.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/hhhh.png")));
		GroupLayout gl_AdminPanel = new GroupLayout(AdminPanel);
		gl_AdminPanel.setHorizontalGroup(
			gl_AdminPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_AdminPanel.createSequentialGroup()
					.addGroup(gl_AdminPanel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_AdminPanel.createSequentialGroup()
							.addGap(19)
							.addGroup(gl_AdminPanel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_AdminPanel.createSequentialGroup()
									.addGap(1)
									.addComponent(lblEnterTheAdministrator))
								.addGroup(gl_AdminPanel.createParallelGroup(Alignment.LEADING)
									.addComponent(label)
									.addComponent(AddBtnAdmin, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
									.addComponent(ListingBtnAdmin, GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)))
							.addGroup(gl_AdminPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_AdminPanel.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 377, GroupLayout.PREFERRED_SIZE)
									.addGap(101))
								.addGroup(gl_AdminPanel.createSequentialGroup()
									.addGap(90)
									.addComponent(LoginTextFieldAdmin, GroupLayout.PREFERRED_SIZE, 216, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
									.addComponent(SearchBtnAdmin, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
									.addGap(57))))
						.addGroup(Alignment.LEADING, gl_AdminPanel.createSequentialGroup()
							.addGap(131)
							.addComponent(lblNewLabel_5, GroupLayout.PREFERRED_SIZE, 388, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_AdminPanel.setVerticalGroup(
			gl_AdminPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_AdminPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_5)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_AdminPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_AdminPanel.createSequentialGroup()
							.addGap(156)
							.addComponent(ListingBtnAdmin)
							.addGap(46)
							.addComponent(label)
							.addGap(34)
							.addComponent(AddBtnAdmin))
						.addGroup(gl_AdminPanel.createSequentialGroup()
							.addGap(12)
							.addGroup(gl_AdminPanel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_AdminPanel.createSequentialGroup()
									.addGroup(gl_AdminPanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblEnterTheAdministrator)
										.addComponent(LoginTextFieldAdmin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addGap(18))
								.addGroup(gl_AdminPanel.createSequentialGroup()
									.addComponent(SearchBtnAdmin, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addComponent(lblNewLabel_2)))
					.addContainerGap(161, Short.MAX_VALUE))
		);
		AdminPanel.setLayout(gl_AdminPanel);
		
		JPanel EmployeePanel = new JPanel();
		EmployeePanel.setBackground(Color.WHITE);
		tabbedPane.addTab("Managing of the employee  accounts", (Icon) null, EmployeePanel, null);
		
		JLabel lblEnterTheEmployee = new JLabel("Enter the employee  login ");
		lblEnterTheEmployee.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		LoginTextFieldEmp = new JTextField();
		LoginTextFieldEmp.setColumns(10);
		
		JButton SearchBtnEmp = new JButton("");
		SearchBtnEmp.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/Serch.jpg")));
		SearchBtnEmp.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if (LoginTextFieldEmp.getText() == null)
					JOptionPane.showMessageDialog(new javax.swing.JDialog(), "Enter an employee login!!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				else {
				
				final Employee found = EmployeeServiceDelegate.findEmployee(LoginTextFieldEmp.getText());
				 if(found==null){
					 JOptionPane.showMessageDialog(new javax.swing.JDialog(), "Inexistant employee!!!", "ERROR",JOptionPane.ERROR_MESSAGE); 
				 }
				 else {
					 java.awt.EventQueue.invokeLater(new Runnable() {
							public void run() {
							new EmployeeDetailsFrame(found).setVisible(true);
							}
							});
				 }
				
			}}
		});
		
		JButton AddBtnEmp = new JButton("Add new employee");
		AddBtnEmp.addActionListener(new ActionListener() {
			
			
			public void actionPerformed(ActionEvent e) {
				AdminFrame.this.dispose();
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new CreateEmployeeFrame(null).setVisible(true);
					}
					});
			}
		});
		
		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/employee.png")));
		
		JLabel lblNewLabel_10 = new JLabel("");
		lblNewLabel_10.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/employeeeeregtrhrtghrt.jpg")));
		GroupLayout gl_EmployeePanel = new GroupLayout(EmployeePanel);
		gl_EmployeePanel.setHorizontalGroup(
			gl_EmployeePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_EmployeePanel.createSequentialGroup()
					.addGroup(gl_EmployeePanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_EmployeePanel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_EmployeePanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_EmployeePanel.createSequentialGroup()
									.addComponent(lblEnterTheEmployee)
									.addGap(36)
									.addComponent(LoginTextFieldEmp, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(SearchBtnEmp, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_EmployeePanel.createSequentialGroup()
									.addComponent(AddBtnEmp, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE)
									.addGap(31)
									.addComponent(lblNewLabel_10, GroupLayout.PREFERRED_SIZE, 408, GroupLayout.PREFERRED_SIZE))))
						.addGroup(gl_EmployeePanel.createSequentialGroup()
							.addGap(179)
							.addComponent(lblNewLabel_4, GroupLayout.PREFERRED_SIZE, 349, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(137, Short.MAX_VALUE))
		);
		gl_EmployeePanel.setVerticalGroup(
			gl_EmployeePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_EmployeePanel.createSequentialGroup()
					.addGroup(gl_EmployeePanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_EmployeePanel.createSequentialGroup()
							.addGap(131)
							.addGroup(gl_EmployeePanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_EmployeePanel.createSequentialGroup()
									.addGap(103)
									.addGroup(gl_EmployeePanel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblEnterTheEmployee)
										.addComponent(LoginTextFieldEmp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_EmployeePanel.createSequentialGroup()
									.addGap(81)
									.addComponent(SearchBtnEmp, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))))
						.addGroup(gl_EmployeePanel.createSequentialGroup()
							.addGap(66)
							.addComponent(lblNewLabel_4)))
					.addGap(42)
					.addGroup(gl_EmployeePanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel_10, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
						.addComponent(AddBtnEmp))
					.addContainerGap(139, Short.MAX_VALUE))
		);
		EmployeePanel.setLayout(gl_EmployeePanel);
		
		JPanel ExpertPanel = new JPanel();
		ExpertPanel.setBackground(Color.WHITE);
		tabbedPane.addTab("Managing of the expert accounts ", null, ExpertPanel, null);
		
		JLabel lblNewLabel = new JLabel("Enter the expert login ");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		LoginTextFieldExp = new JTextField();
		LoginTextFieldExp.setColumns(10);
		
		JButton SearchBtnExp = new JButton("");
		SearchBtnExp.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/Serch.jpg")));
		SearchBtnExp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(LoginTextFieldExp.getText() == null)
					JOptionPane.showMessageDialog(new javax.swing.JDialog(), "Enter an expert login !!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				else {
				final Expert found= ExpertServiceDelegate.findExpert(LoginTextFieldExp.getText().toString());
			
				 if(found==null){
					 JOptionPane.showMessageDialog(new javax.swing.JDialog(), "Inexistant expert!!!", "ERROR",JOptionPane.ERROR_MESSAGE); 
				 }
				 else {
					 java.awt.EventQueue.invokeLater(new Runnable() {
							public void run() {
							new ExpertDetailsFrame(found).setVisible(true);
							}
							});
				 }}
			}
		});
		
		JButton ListingBtnExp = new JButton("Show All");
		ListingBtnExp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new ExpertListFrame(admin).setVisible(true);
					}
					});
			}
		});
		
		JButton AddBtnExp = new JButton("Add new ");
		AddBtnExp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new CreateExpertFrame().setVisible(true);
					}
					});
			}
		});
		
		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/Expert.png")));
		
		JLabel lblNewLabel_11 = new JLabel("New label");
		lblNewLabel_11.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/Experrttt.jpg")));
		GroupLayout gl_ExpertPanel = new GroupLayout(ExpertPanel);
		gl_ExpertPanel.setHorizontalGroup(
			gl_ExpertPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_ExpertPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_ExpertPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_ExpertPanel.createSequentialGroup()
							.addGap(9)
							.addGroup(gl_ExpertPanel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(AddBtnExp, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(ListingBtnExp, GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
							.addComponent(lblNewLabel_11, GroupLayout.PREFERRED_SIZE, 466, GroupLayout.PREFERRED_SIZE)
							.addGap(101))
						.addGroup(gl_ExpertPanel.createSequentialGroup()
							.addComponent(lblNewLabel)
							.addGap(34)
							.addComponent(LoginTextFieldExp, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(SearchBtnExp, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))))
				.addGroup(gl_ExpertPanel.createSequentialGroup()
					.addGap(201)
					.addComponent(lblNewLabel_7)
					.addContainerGap(232, Short.MAX_VALUE))
		);
		gl_ExpertPanel.setVerticalGroup(
			gl_ExpertPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_ExpertPanel.createSequentialGroup()
					.addGap(23)
					.addComponent(lblNewLabel_7)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_ExpertPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_ExpertPanel.createSequentialGroup()
							.addGap(67)
							.addGroup(gl_ExpertPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel)
								.addComponent(LoginTextFieldExp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_ExpertPanel.createSequentialGroup()
							.addGap(57)
							.addComponent(SearchBtnExp, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)))
					.addGroup(gl_ExpertPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_ExpertPanel.createSequentialGroup()
							.addGap(75)
							.addComponent(ListingBtnExp))
						.addGroup(gl_ExpertPanel.createSequentialGroup()
							.addGap(32)
							.addGroup(gl_ExpertPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_11)
								.addComponent(AddBtnExp))))
					.addContainerGap(114, Short.MAX_VALUE))
		);
		ExpertPanel.setLayout(gl_ExpertPanel);
		
		JPanel EnterprisePanel = new JPanel();
		EnterprisePanel.setBackground(Color.WHITE);
		tabbedPane.addTab("Enterprise Management ", null, EnterprisePanel, null);
		
		JLabel lblNewLabel_1 = new JLabel("Enter the enterprise  name");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		nameTextFieldEnt = new JTextField();
		nameTextFieldEnt.setColumns(10);
		
		JButton SearchBtnEnt = new JButton("");
		SearchBtnEnt.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/Serch.jpg")));
		SearchBtnEnt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(nameTextFieldEnt.getText()==null)
					JOptionPane.showMessageDialog(new javax.swing.JDialog(), "Enter an enterprise name!!!", "ERROR",JOptionPane.ERROR_MESSAGE);
				else{
				
				final Enterprise ent=EnterpriseServiceDelegate.findEnterpriseByName(nameTextFieldEnt.getText());
				
				 if(ent==null)
					 JOptionPane.showMessageDialog(new javax.swing.JDialog(), "Inexistant enterprise !!!","ERROR",JOptionPane.ERROR_MESSAGE); 
				 
				 else {
					 
					 java.awt.EventQueue.invokeLater(new Runnable() {
							public void run() {
							new EnterpriseDetailsFrame(ent).setVisible(true);
							}
							});
				 }
				
			}}
		});
		
		JButton ListingBtnEnt = new JButton("Show all enterprises");
		ListingBtnEnt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new EnterpriseListFrame().setVisible(true);
					}
					});
			}
		});
		
		JButton AddBtnEnt = new JButton("Add new enterprise");
		AddBtnEnt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				 java.awt.EventQueue.invokeLater(new Runnable() {
						public void run() {
						new CreateEnterpriseFrame(admin).setVisible(true);
						}
						});
			}
		});
		panel.setLayout(null);
		
		JLabel lblNewLabel_8 = new JLabel("");
		lblNewLabel_8.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/entre.png")));
		
		JLabel lblNewLabel_12 = new JLabel("");
		lblNewLabel_12.setIcon(new ImageIcon(AdminFrame.class.getResource("/edu/app/iso/icone/Enterprise.jpg")));
		GroupLayout gl_EnterprisePanel = new GroupLayout(EnterprisePanel);
		gl_EnterprisePanel.setHorizontalGroup(
			gl_EnterprisePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_EnterprisePanel.createSequentialGroup()
					.addGap(37)
					.addGroup(gl_EnterprisePanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_EnterprisePanel.createSequentialGroup()
							.addComponent(lblNewLabel_1)
							.addGap(32)
							.addComponent(nameTextFieldEnt, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
							.addGap(39)
							.addComponent(SearchBtnEnt, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_EnterprisePanel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_EnterprisePanel.createSequentialGroup()
								.addComponent(ListingBtnEnt)
								.addContainerGap())
							.addGroup(gl_EnterprisePanel.createSequentialGroup()
								.addComponent(AddBtnEnt, GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE)
								.addGap(90)
								.addComponent(lblNewLabel_12, GroupLayout.PREFERRED_SIZE, 273, GroupLayout.PREFERRED_SIZE)
								.addGap(214)))))
				.addGroup(gl_EnterprisePanel.createSequentialGroup()
					.addGap(159)
					.addComponent(lblNewLabel_8)
					.addContainerGap(267, Short.MAX_VALUE))
		);
		gl_EnterprisePanel.setVerticalGroup(
			gl_EnterprisePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_EnterprisePanel.createSequentialGroup()
					.addGap(72)
					.addComponent(lblNewLabel_8)
					.addGap(72)
					.addGroup(gl_EnterprisePanel.createParallelGroup(Alignment.TRAILING, false)
						.addGroup(gl_EnterprisePanel.createParallelGroup(Alignment.BASELINE)
							.addComponent(lblNewLabel_1)
							.addComponent(nameTextFieldEnt, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(SearchBtnEnt, GroupLayout.PREFERRED_SIZE, 51, Short.MAX_VALUE))
					.addGroup(gl_EnterprisePanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_EnterprisePanel.createSequentialGroup()
							.addGap(57)
							.addComponent(ListingBtnEnt)
							.addGap(55)
							.addComponent(AddBtnEnt))
						.addGroup(gl_EnterprisePanel.createSequentialGroup()
							.addGap(95)
							.addComponent(lblNewLabel_12, GroupLayout.PREFERRED_SIZE, 202, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(136, Short.MAX_VALUE))
		);
		EnterprisePanel.setLayout(gl_EnterprisePanel);
		panel.add(tabbedPane);
	}
}
