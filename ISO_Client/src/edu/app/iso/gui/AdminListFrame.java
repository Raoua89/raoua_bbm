package edu.app.iso.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JTableBinding;
import org.jdesktop.swingbinding.SwingBindings;

import edu.app.iso.delegate.AdminServiceDelegate;
import edu.app.iso.persistence.Administrator;

public class AdminListFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	private List<Administrator> admins;
	private JTable table;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminListFrame frame = new AdminListFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminListFrame() {
		
		
		
		admins= AdminServiceDelegate.findAllAdmins();
		setTitle("Administrators List");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel masterPanel = new JPanel();
		masterPanel.setBorder(new TitledBorder(null, "select an admin to show its details", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JButton backBtn = new JButton("<<back");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdminListFrame.this.dispose();
				java.awt.EventQueue.invokeLater(new Runnable() {
					public void run() {
					new AdminFrame().setVisible(true);
					}
					});	
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(masterPanel, GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
						.addComponent(backBtn))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(masterPanel, GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(backBtn)
					.addContainerGap(24, Short.MAX_VALUE))
		);
		
		JScrollPane scrollPane = new JScrollPane();
		GroupLayout gl_masterPanel = new GroupLayout(masterPanel);
		gl_masterPanel.setHorizontalGroup(
			gl_masterPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_masterPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_masterPanel.setVerticalGroup(
			gl_masterPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_masterPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		masterPanel.setLayout(gl_masterPanel);
		contentPane.setLayout(gl_contentPane);
		initDataBindings();
	}
	protected void initDataBindings() {
		JTableBinding<Administrator, List<Administrator>, JTable> jTableBinding = SwingBindings.createJTableBinding(UpdateStrategy.READ, admins, table);
		//
		BeanProperty<Administrator, String> administratorBeanProperty = BeanProperty.create("login");
		jTableBinding.addColumnBinding(administratorBeanProperty).setColumnName("Login").setEditable(false);
		//
		BeanProperty<Administrator, String> administratorBeanProperty_1 = BeanProperty.create("name");
		jTableBinding.addColumnBinding(administratorBeanProperty_1).setColumnName("Name").setEditable(false);
		//
		
		table.setCellSelectionEnabled(true);
		ListSelectionModel cellSelectionModel = table.getSelectionModel();
		cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		cellSelectionModel.addListSelectionListener(new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {

		String selectedData = null;
		int selectedRow = table.getSelectedRow ();
		selectedData = (String) table.getValueAt(selectedRow,0);

		
		 System.out.println(selectedData);
		final Administrator selectedAdmin=AdminServiceDelegate.findAdmin(selectedData);
		
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
			new AdminDetailsFrame(selectedAdmin).setVisible(true);
			}
			});	
		




		}
		});


		
		
		jTableBinding.setEditable(false);
		jTableBinding.bind();
	}
}
