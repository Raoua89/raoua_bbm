package edu.app.iso.gui;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class HelpFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public HelpFrame() {
		
		setTitle("Help ");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JTextArea textArea = new JTextArea();
		textArea.setText("Welcome in Iso Quality Management System! \n"+"If you have a login and a password enter them to connect to the platform\n"+"If you don't, please check your e-mail to find them.\n" +
				"If you have forgot your password select the option below to send it to your e-mail adress." );
		
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 414, Short.MAX_VALUE)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(textArea, GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(84, Short.MAX_VALUE))
		);
		getContentPane().setLayout(groupLayout);
	}
}
