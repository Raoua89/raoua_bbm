package edu.app.iso.session;

import java.util.HashMap;
import java.util.Map;

public class Session {
	
	static Session instance=new Session();
	
	public static Session getInstance(){
		return instance;
	}
	
	private Map<String, Object> map=new HashMap<String, Object>();
	
	public Session() {
	}
	
	public void setParametre(String name,Object value){
		
	if(map.containsKey(name)){
		map.remove(name);
	}	
	
	map.put(name, value);
		
	}
	

	
	
	public Object getParametre(String name) {
		return map.get(name);
	}
	
}
