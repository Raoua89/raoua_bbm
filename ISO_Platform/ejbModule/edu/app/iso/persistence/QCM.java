package edu.app.iso.persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: QCM
 *
 */
@Entity
@Table(name="t_QCM")
@NamedQueries({
    @NamedQuery(name = "QCM.findAll", query =   "SELECT DISTINCT q FROM QCM q")})

public class QCM implements Serializable {

	
	private int qcmId;
	private Date date;
	private String difficultyLevel;
	private static int nbQuestions=10 ;
	private int finalScore;
	private Employee employee;
	private List<Score> scores;
	private static final long serialVersionUID = 1L;

	public QCM() {
		
	}   
	public QCM(Employee employee,String difficultyLevel, Date date,int finalScore) {
		this.difficultyLevel = difficultyLevel;
		this.finalScore=finalScore;
		this.date=date;
		this.employee=employee;
	}
	@Id 
	@GeneratedValue
	public int getQcmId() {
		return this.qcmId;
	}

	public void setQcmId(int qcmId) {
		this.qcmId = qcmId;
	}   
	public String getDifficultyLevel() {
		return this.difficultyLevel;
	}

	public void setDifficultyLevel(String difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}
	
	
	@OneToMany(mappedBy="qcm", cascade=CascadeType.ALL)
	public List<Score> getScores() {
		return scores;
	}
	public void setScores(List<Score> scores) {
		this.scores = scores;
	}
	
	public void addScore(Score score) {
		this.scores.add(score);
	}
	
	
	public int getFinalScore() {
		return finalScore;
	}
	public void setFinalScore(int finalScore) {
		this.finalScore = finalScore;
	}
	public static int getNbQuestions() {
		return QCM.nbQuestions;
	}
	public static void setNbQuestions(int nbQuestions) {
		QCM.nbQuestions = nbQuestions;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@ManyToOne
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	
   
}
