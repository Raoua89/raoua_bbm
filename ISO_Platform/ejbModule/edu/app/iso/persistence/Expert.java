package edu.app.iso.persistence;



import javax.persistence.*;

/**
 * Entity implementation class for Entity: Expert
 *
 */
@Entity
@Table(name="t_expert")

@NamedQueries({
    @NamedQuery(name = "Expert.findAll", query = "SELECT ex FROM Expert ex"),
    @NamedQuery(name = "Expert.authenticate", query = "select ex from Expert ex where ex.login=:param1 and ex.password=:param2")
  })


public class Expert extends User {

	
	
	private static final long serialVersionUID = 1L;

	public Expert() {
		
	} 
	public Expert(String login, String password, String name, String eMail,
		int phoneNumber){
		super(login,password,name,eMail,phoneNumber);
	}
	
	
	
	}
   

