package edu.app.iso.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ScorePK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idQcm;
	private int idQuestion;
	
	public ScorePK() {
		
	}

	public ScorePK(int qcm, int question) {
		super();
		this.idQcm = qcm;
		this.idQuestion = question;
	}

	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idQcm;
		result = prime * result + idQuestion;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScorePK other = (ScorePK) obj;
		if (idQcm != other.idQcm)
			return false;
		if (idQuestion != other.idQuestion)
			return false;
		return true;
	}
    @Column(name="qcm_fk")
	public int getQcm() {
		return idQcm;
	}

	public void setQcm(int qcm) {
		this.idQcm = qcm;
	}
   @Column(name="question_fk")
	public int getQuestion() {
		return idQuestion;
	}

	public void setQuestion(int question) {
		this.idQuestion = question;
	}

}
