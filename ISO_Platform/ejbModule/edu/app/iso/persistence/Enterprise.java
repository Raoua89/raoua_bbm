package edu.app.iso.persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Enterprise
 *
 */
@Entity
@Table(name="t_enterprise")
@NamedQueries({
    @NamedQuery(name = "Enterprise.findAll", query = "SELECT ent FROM Enterprise ent"),
    @NamedQuery(name = "Enterprise.findById", query = "SELECT ent FROM Enterprise ent WHERE ent.id = :param"),
    @NamedQuery(name = "Employer.findByNom", query = "SELECT ent FROM Enterprise ent WHERE ent.name = :param")
   })
public class Enterprise implements Serializable {

	
	private int id;
	private String name;
	private String adress;
	private BusinessArea businessArea;
	private String category;
	
	
	

	private List<Employee> employees = new ArrayList<Employee>();
	
	private static final long serialVersionUID = 1L;
	public Enterprise() {
		
	} 
	
	
	public Enterprise(String name, String adress, BusinessArea businessArea,String category,
			List<Employee> employees) {
		
		
		this.name= name;
		this.adress = adress;
		this.businessArea = businessArea;
		this.category = category;
		this.employees = employees;
	}


		
	@Id   
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getAdress() {
		return this.adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}   
	
	@Enumerated(EnumType.STRING)
	public BusinessArea getBusinessArea() {
		return this.businessArea;
	}

	public void setBusinessArea(BusinessArea businessArea) {
		this.businessArea = businessArea;
	}

	@OneToMany(mappedBy="enterprise",cascade=CascadeType.REMOVE)
	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public void addEmployee(Employee employee) {
		employees.add(employee);}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}
}
	
	