package edu.app.iso.persistence;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Response
 *
 */
@Entity
@Table(name="t_response")
@NamedQueries({
    @NamedQuery(name = "Response.findAll", query = "SELECT res FROM Response res"),
    @NamedQuery(name = "Response.findByQcm", query = "SELECT res FROM Response res WHERE res.qcm= :param "),
    @NamedQuery(name = "Question.fetch", query = "SELECT res FROM Response res WHERE res.qcm= :param1 AND res.question= :param2"),
    @NamedQuery(name = "Response.findByQuestion", query = "SELECT res FROM Response res WHERE res.question= :param ")
})
public class Response implements Serializable {

	
	private int responseId;
	private Question question;
	private QCM qcm;
	private Boolean value1;
	private Boolean value2;
	private Boolean value3;
	private Boolean value4;
	
	private static final long serialVersionUID = 1L;

	public Response() {
		
	}   
	public Response(Question question) {
		
		this.question = question;
		this.setValue1(null);
		this.setValue2(null);
		this.setValue3(null);
		this.setValue4(null);
		
		
	}
	@Id 
	@GeneratedValue
	public int getId() {
		return this.responseId;
	}

	public void setId(int responseId) {
		this.responseId = responseId;
	}
	
	
	
	
	@ManyToOne
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	
	
	@ManyToOne
	public QCM getQcm() {
		return qcm;
	}
	public void setQcm(QCM qcm) {
		this.qcm = qcm;
	}
	public Boolean getValue1() {
		return value1;
	}
	public void setValue1(Boolean value1) {
		this.value1 = value1;
	}
	public Boolean getValue2() {
		return value2;
	}
	public void setValue2(Boolean value2) {
		this.value2 = value2;
	}
	public Boolean getValue3() {
		return value3;
	}
	public void setValue3(Boolean value3) {
		this.value3 = value3;
	}
	public Boolean getValue4() {
		return value4;
	}
	public void setValue4(Boolean value4) {
		this.value4 = value4;
	}
	
}
