package edu.app.iso.persistence;

import java.io.Serializable;
import java.lang.Boolean;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Proposal
 *
 */
@Entity
@Table(name="t_proposal")
@NamedQueries({
    @NamedQuery(name = "Proposal.find", query = "SELECT p FROM Proposal p WHERE p.question = :param"),
    @NamedQuery(name = "Proposal.findAll", query = "SELECT p FROM Proposal p ")
})
public class Proposal implements Serializable {

	
	private int proposalId;
	private String proposalText;
	private Boolean proposalState;
	private Boolean responseState;
	private Question question;
	private static final long serialVersionUID = 1L;

	public Proposal() {
		
	}  
	
	
	@Id
	@GeneratedValue
	public int getProposalId() {
		return proposalId;
	}

public Proposal(String proposalText, Boolean proposalState,
			Question question, Response response) {
		super();
		this.proposalText = proposalText;
		this.proposalState = proposalState;
		this.question = question;
	}


public void setProposalId(int proposalId) {
		this.proposalId = proposalId;
	}
@ManyToOne
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	
	
	public String getProposalText() {
		return proposalText;
	}
	public void setProposalText(String proposalText) {
		this.proposalText = proposalText;
	}
	public Boolean getProposalState() {
		return proposalState;
	}
	public void setProposalState(Boolean proposalState) {
		this.proposalState = proposalState;
	}


	public Boolean getResponseState() {
		return responseState;
	}


	public void setResponseState(Boolean responseState) {
		this.responseState = responseState;
	}
   
}
