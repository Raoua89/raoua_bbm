package edu.app.iso.persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Question
 *
 */
@Entity
@Table(name="t_question")

@NamedQueries({
    @NamedQuery(name = "Question.findAll", query = "SELECT q FROM Question q WHERE q.course = :param1 AND q.difficultyLevel = :param2"),
    @NamedQuery(name = "Question.findAllQuestions", query = "SELECT q FROM Question q WHERE q.course = :param ")
})

public class Question implements Serializable {

	
	private int questionId;
	private String questionText;
	private String difficultyLevel;
	private List<Response> responses;
	private List<Score> scores;
	private List<Proposal> proposals;
	private Course course;
	private static final long serialVersionUID = 1L;

	public Question() {
		
	}   
	@Id
	@GeneratedValue
	public int getQuestionId() {
		return this.questionId;
	}

	public Question(String questionText, String difficultyLevel, Course course) {
		this.questionText = questionText;
		this.difficultyLevel = difficultyLevel;
		this.course=course;
		this.proposals=null;
		this.responses=null;
		
	}
	public void setQuestionId(int QuestionId) {
		this.questionId = QuestionId;
	}   
	public String getQuestionText() {
		return this.questionText;
	}

	public void setQuestionText(String QuestionText) {
		this.questionText = QuestionText;
	}
	
	
	public String getDifficultyLevel() {
		return difficultyLevel;
	}
	public void setDifficultyLevel(String difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}
	
	
	@OneToMany(mappedBy="question",cascade=CascadeType.ALL)
	public List<Proposal> getProposals() {
		return proposals;
	}
	public void setProposals(List<Proposal> proposals) {
		this.proposals = proposals;
	}
	
	public void addProposal(Proposal proposal) {
		this.proposals.add(proposal);
	}
	
	
	@OneToMany(mappedBy="question",cascade=CascadeType.PERSIST)
	public List<Score> getScores() {
		return scores;
	}
	public void setScores(List<Score> scores) {
		this.scores = scores;
	}
	
	@ManyToOne
	@JoinColumn(name="course_fk")
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	
	@OneToMany(mappedBy="question",cascade=CascadeType.REMOVE)
	public List<Response> getResponses() {
		return responses;
	}
	public void setResponses(List<Response> responses) {
		this.responses = responses;
	}
   
	public void addResponse(Response response) {
		this.responses.add(response);
	}
}
