package edu.app.iso.persistence;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Score
 *
 */
@Entity
@Table(name="t_score")
@NamedQueries({
    @NamedQuery(name = "Score.findByEmployee", query = "SELECT s FROM Score s  join s.qcm quiz  WHERE quiz.employee = :p ")
})
public class Score implements Serializable {

	private ScorePK pk;
	private int value;
	private QCM qcm;
	private Question question;
	private static final long serialVersionUID = 1L;

	public Score() {
		
	}  
	
	
	
	
	public Score(QCM qcm, Question question, int value){
		this.getPk().setQcm(qcm.getQcmId());
		this.getPk().setQuestion(question.getQuestionId());
		this.setValue(value);
		this.setQcm(qcm);
		this.setQuestion(question);
	}
	public int getValue() {
		return this.value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	@EmbeddedId
	public ScorePK getPk() {
		return pk;
	}
	public void setPk(ScorePK pk) {
		this.pk = pk;
	}
	
	@ManyToOne
	@JoinColumn(name="qcm_fk",insertable=false,updatable=false)
	public QCM getQcm() {
		return qcm;
	}
	public void setQcm(QCM qcm) {
		this.qcm = qcm;
	}
	
	
	@ManyToOne
	@JoinColumn(name="question_fk",insertable=false,updatable=false)
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
   
}
