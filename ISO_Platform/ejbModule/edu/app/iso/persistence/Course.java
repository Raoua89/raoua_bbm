package edu.app.iso.persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Course
 *
 */
@Entity
@Table(name="t_course")
@NamedQueries({
    @NamedQuery(name = "Course.findAll", query =   "SELECT c FROM Course c "),
    @NamedQuery(name = "Course.findByBusinessArea", query =   "SELECT c FROM Course c WHERE c.businessArea = :param "),
    @NamedQuery(name = "Course.findByTitle", query =   "SELECT c FROM Course c WHERE c.title = :param ")
    })

public class Course implements Serializable {

	
	private int courseId;
	private BusinessArea businessArea;
	private List<Question> questions; 
	private String title;
	
	private byte[] data;
	
	private static final long serialVersionUID = 1L;

	public Course() {
		
	} 
	
	public Course(BusinessArea businessArea,String title,byte[] data) {
		
		this.businessArea = businessArea;
		this.title=title;
		this.data=data;
	}

	@Id 
	@GeneratedValue
	public int getCourseId() {
		return this.courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public BusinessArea getBusinessArea() {
		return businessArea;
	}
	public void setBusinessArea(BusinessArea businessArea) {
		this.businessArea = businessArea;
	}
	
	
	public void addQuestion(Question question) {
		this.questions.add (question);
	}
   @OneToMany(mappedBy="course",cascade=CascadeType.REMOVE)
	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Basic
	@Lob
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
	
   
}
