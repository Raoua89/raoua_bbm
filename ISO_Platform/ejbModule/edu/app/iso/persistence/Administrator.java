package edu.app.iso.persistence;



import javax.persistence.*;

/**
 * Entity implementation class for Entity: Administrator
 *
 */
@Entity
@Table(name="t_admin")

@NamedQueries({
    @NamedQuery(name = "Admin.findAll", query = "SELECT a FROM Administrator a"),
    @NamedQuery(name = "Admin.authenticate", query = "select a from Administrator a where a.login=:param1 and a.password=:param2")
   })
public class Administrator extends User  {


	


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Administrator() {
		
	}
	
	public Administrator(String login,String password, String name, String eMail,int phoneNumber){
		super(login,password,name,eMail,phoneNumber);
		}
	
	

	
	

   
}
