package edu.app.iso.persistence;

public enum BusinessArea {
	BanksInsuranceAndFinancialServices,
	TradingAndDistribution,
	PrintingMediaAndCommunication,
	EducationTrainingAndConsulting,
	ComputerScienceTelecommunicationsAndMultimedia,
	Services,
	TourismAndTravel 

}
