package edu.app.iso.persistence;


import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Employee
 *
 */
@Entity
@Table(name="t_employee")

@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "SELECT e FROM Employee e WHERE e.enterprise = :param"),
    @NamedQuery(name = "Employee.findAllEmployees", query = "SELECT e FROM Employee e "),
    @NamedQuery(name = "Employee.authenticate", query = "select emp from Employee emp where emp.login=:param1 and emp.password=:param2")
})
public class Employee extends User  {

	
	
	
    private int timeLeft;
    
	private String speciality;
	
	private Enterprise enterprise;
	private List<QCM> qcms;

	private static final long serialVersionUID = 1L;

	public Employee() {
		
	} 
	
	
	  
public Employee(String login, String password, String name, String eMail,
		int phoneNumber,int timeLeft, String speciality, Enterprise enterprise) {
		super(login,password,name,eMail,phoneNumber);
		this.timeLeft = timeLeft;
		this.speciality = speciality;
		this.enterprise = enterprise;
	}



public void setTimeLeft(int timeLeft) {
	this.timeLeft = timeLeft;
}

public int getTimeLeft() {
	return timeLeft;
}
	public String getSpeciality() {
		return this.speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
    @ManyToOne
    @JoinColumn(name="enterprise_fk")
	public Enterprise getEnterprise() {
		return enterprise;
	}
	
	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}


	
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((enterprise == null) ? 0 : enterprise.hashCode());
		result = prime * result
				+ ((speciality == null) ? 0 : speciality.hashCode());
		return result;
	}


	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (enterprise == null) {
			if (other.enterprise != null)
				return false;
		} else if (!enterprise.equals(other.enterprise))
			return false;
		if (speciality == null) {
			if (other.speciality != null)
				return false;
		} else if (!speciality.equals(other.speciality))
			return false;
		return true;
	}


	
	public String toString() {
		return "Employee ["+ super.toString()+ ", speciality=" + speciality
				+ ", enterprise=" + enterprise +  "]";
	}
	


   @OneToMany(mappedBy="employee", cascade=CascadeType.REMOVE)
	public List<QCM> getQcms() {
		return qcms;
	}

   public void setQcms(List<QCM> qcms) {
	   this.qcms = qcms;
   }
	

   
}
