package edu.app.iso.persistence;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: User
 *
 */
@MappedSuperclass


public abstract class User implements Serializable {

	
	private String login;
	private String password;
	private String name;
	private String email;
	


	public String getEmail() {
		return email;
	}


	public void setEmail(String eMail) {
		this.email = eMail;
	}


	private int phoneNumber;
	
	private static final long serialVersionUID = 1L;

	public User() {
	}
	
	
	public User(String login, String password, String name, String eMail,
			int phoneNumber) {
		
		this.login = login;
		this.password = password;
		this.name = name;
		this.email = eMail;
		this.phoneNumber = phoneNumber;
	}


	@Id    
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
	public int getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	

	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		return result;
	}

	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}


	public String toString() {
		return "User [login=" + login + ", name=" + name + ", eMail=" + email
				+ ", phoneNumber=" + phoneNumber + "]";
	}
	
	

	
   
}
