package edu.app.iso.timer;

import javax.ejb.Remote;

@Remote
public interface MyTimerRemote {

	public void start(int time);

}
