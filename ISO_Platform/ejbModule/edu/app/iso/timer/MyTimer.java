package edu.app.iso.timer;

import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateful;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.enterprise.event.Event;
import javax.inject.Inject;

@Stateful(name="TimerService")
public class MyTimer implements MyTimerRemote{

	@Inject
	Event<String> event;
	
	//@Resource
	//TimerService timerService;

	
	
	@Timeout
	public void timeOut(Timer timer) {
		
		event.fire("time is up !!");
		
	}


	public void start(int time) {
		java.util.Timer t= new java.util.Timer();
		ScheduleExpression  expression= new ScheduleExpression();
		
		expression.minute(time);
		
		//timerService.createCalendarTimer(expression, null);
		
	}

	
	
	
}
