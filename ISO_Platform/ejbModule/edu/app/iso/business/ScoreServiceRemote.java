package edu.app.iso.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Score;

@Remote
public interface ScoreServiceRemote extends AbstractFacadeRemote<Score>{

	List<Score> findAllScores(Employee emp);

}
