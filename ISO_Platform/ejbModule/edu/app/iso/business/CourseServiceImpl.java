package edu.app.iso.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.iso.persistence.Course;

/**
 * Session Bean implementation class CourseServiceImpl
 */
@Stateless(name="CourseService")
public class CourseServiceImpl extends AbstractFacade<Course>implements CourseServiceRemote {
	@PersistenceContext( unitName = "IsoPlatformPU" )
  private EntityManager em;
  
  
    public EntityManager getEm() {
	return em;
}


public void setEm(EntityManager em) {
	this.em = em;
}


	public CourseServiceImpl() {
       super(Course.class);
    }

	
	@SuppressWarnings("unchecked")
	public List<Course> findAll() {
		return em.createNamedQuery("Course.findAll").getResultList();
	}


	
	
	@SuppressWarnings("unchecked")
	public List<Course> findByBusinessArea(String bus ) {
		return  em.createNamedQuery("Course.findByBusinessArea").setParameter("param", bus).getResultList();
	}


	
	public Course findByTitle(String title) {
		return (Course) em.createNamedQuery("Course.findByTitle").setParameter("param", title).getSingleResult();
	}



	}


