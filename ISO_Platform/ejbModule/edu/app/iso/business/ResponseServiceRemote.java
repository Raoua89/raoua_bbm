package edu.app.iso.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Question;
import edu.app.iso.persistence.Response;

@Remote
public interface ResponseServiceRemote extends AbstractFacadeRemote<Response>{

	List<Response> findAll(Question question);

	List<Response> findResponses(Question question, Employee employee);

}
