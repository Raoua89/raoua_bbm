package edu.app.iso.business;

import javax.ejb.Remote;

@Remote
public interface AbstractFacadeRemote<T> {
	
	void create(T entity);
	T find(Object id);
	void edit(T entity);
	void remove(T entity);


}
