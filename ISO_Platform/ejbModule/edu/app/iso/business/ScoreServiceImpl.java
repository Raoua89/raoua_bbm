package edu.app.iso.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Score;

/**
 * Session Bean implementation class ScoreServiceImpl
 */
@Stateless(name="ScoreService")
public class ScoreServiceImpl extends AbstractFacade<Score> implements ScoreServiceRemote {
	@PersistenceContext( unitName = "IsoPlatformPU" )
   private EntityManager em;
    public EntityManager getEm() {
	return em;
}

public void setEm(EntityManager em) {
	this.em = em;
}

	public ScoreServiceImpl() {
       super(Score.class);
    }

	
	@SuppressWarnings("unchecked")
	public List<Score> findAllScores(Employee emp) {
		
		return em.createNamedQuery("Score.findByEmployee").setParameter("p", emp).getResultList();
	}

}
