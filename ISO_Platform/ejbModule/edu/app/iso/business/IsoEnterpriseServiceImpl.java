package edu.app.iso.business;

import java.util.List;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.iso.persistence.Enterprise;



/**
 * Session Bean implementation class IsoEnterpriseServiceImpl
 */
@Stateless(name="EnterpriseService")
public class IsoEnterpriseServiceImpl implements IsoEnterpriseServiceRemote {
	
	
	@PersistenceContext(unitName="IsoPlatformPU")
	 private EntityManager em;

    /**
     * Default constructor. 
     */
    public IsoEnterpriseServiceImpl() {
      
    }

	
	public void createEnterprise(Enterprise ent) {
		em.persist(ent);
		
	}

	
	public Enterprise findEnterprise(int id) {
		return em.find(Enterprise.class, id);
	}
	
	

	
	public void updateEnterprise(Enterprise ent) {
		em.merge(ent);
		
	}

	
	public void removeEnterprise(Enterprise ent) {
		em.remove(em.merge(ent));
	}

	
	@SuppressWarnings("unchecked")
	public List<Enterprise> findAllEnterprises() {
		 return em.createQuery("select ent from Enterprise ent order by ent.name asc").getResultList();
		
		
	}



	public Enterprise findEnterpriseByName(String name) {
		Enterprise found = null;
		try{
		 found = (Enterprise) em.createQuery("select ent from Enterprise ent where ent.name=:param").setParameter("param",name).getSingleResult();
		}
		catch(Exception e){
			
		}
		return found;
		
	}

}
