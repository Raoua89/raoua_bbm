package edu.app.iso.business;

import java.util.Date;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Session Bean implementation class EmailSender
 */
@Stateless
public class EmailSender implements EmailSenderRemote {
	

    /**
     * Default constructor. 
     */
    public EmailSender() {
        // TODO Auto-generated constructor stub
    }
    
    public void sendEMail(String sender, String receiver,String text)  {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.topnet.tn");

        Session session = Session.getInstance(properties, null);

        Message msg = new MimeMessage(session);
			
		try {
			msg.setFrom(new InternetAddress(sender));
			msg.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(receiver, false));
			msg.setSubject("Platform Of quality management ");
			msg.setText(text);
			msg.setSentDate(new Date());
			Transport.send(msg);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
    }

        

}
