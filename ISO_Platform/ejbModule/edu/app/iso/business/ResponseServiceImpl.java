package edu.app.iso.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.iso.persistence.Employee;
import edu.app.iso.persistence.Question;
import edu.app.iso.persistence.Response;

/**
 * Session Bean implementation class ResponseServiceImpl
 */
@Stateless
public class ResponseServiceImpl extends AbstractFacade<Response> implements ResponseServiceRemote {
	@PersistenceContext( unitName = "IsoPlatformPU" )
   private EntityManager em;
    public EntityManager getEm() {
	return em;
}

public void setEm(EntityManager em) {
	this.em = em;
}

	public ResponseServiceImpl() {
        super(Response.class);
    }

	@SuppressWarnings("unchecked")
	public List<Response> findAll(Question question) {
		return em.createNamedQuery("Response.findAll").getResultList();
	}


	@SuppressWarnings("unchecked")
	public List<Response> findResponses(Question question, Employee employee) {
		return em.createNamedQuery("Response.findByEmployee").setParameter("param", employee.getLogin()).getResultList();
	}

}
