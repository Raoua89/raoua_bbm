package edu.app.iso.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.iso.persistence.Course;
import edu.app.iso.persistence.QCM;
import edu.app.iso.persistence.Question;
import edu.app.iso.persistence.Response;

@Remote
public interface QuestionServiceRemote extends AbstractFacadeRemote<Question> {
	public List<Question> findQuestions(Course course, String dificulty);
	 public Question chooseQuestion(List<Question> list);
	public List<Question> findAllQuestions(Course course);
	public Response fetchResponse(Question question, QCM qcm);
	
}
