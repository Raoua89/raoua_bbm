package edu.app.iso.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.iso.persistence.Proposal;
import edu.app.iso.persistence.Question;

/**
 * Session Bean implementation class ProposalServiceImpl
 */
@Stateless(name="ProposalService")
public class ProposalServiceImpl extends AbstractFacade<Proposal> implements ProposalServiceRemote {
	@PersistenceContext( unitName = "IsoPlatformPU" )
   private EntityManager em;
    public EntityManager getEm() {
	return em;
}


public void setEm(EntityManager em) {
	this.em = em;
}


	public ProposalServiceImpl() {
       super(Proposal.class);
    }

	
	@SuppressWarnings("unchecked")
	public List<Proposal> findAllProposals() {
		
		return em.createNamedQuery("Proposal.findAll").getResultList();
	}


	@SuppressWarnings("unchecked")
	public List<Proposal> findProposals(Question question) {
		return em.createNamedQuery("Proposal.find").setParameter("param", question).getResultList();
	}

}
