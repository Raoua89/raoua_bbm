package edu.app.iso.business;



import javax.persistence.EntityManager;

public abstract class  AbstractFacade<T> {

	/**
	 * @param args
	 */
	
	
	  private Class<T> entityClass;

	    public AbstractFacade(Class<T> entityClass) {
	        this.entityClass = entityClass;
	    }

	    protected abstract EntityManager getEm();

	    public void create(T entity) {
	    	getEm().persist(entity);
	    }

	    public void edit(T entity) {
	    	getEm().merge(entity);
	    }

	    public void remove(T entity) {
	    	getEm().remove(getEm().merge(entity));
	    }

	    public T find(Object id) {
	        return getEm().find(entityClass, id);
	    }
        
	
	  

	
}
