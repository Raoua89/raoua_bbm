package edu.app.iso.business;


import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.iso.persistence.Administrator;

/**
 * Session Bean implementation class IsoAdminServiceImpl
 */
@Stateless(name="AdminService")
public class IsoAdminServiceImpl extends AbstractFacade<Administrator> implements IsoUserServiceRemote<Administrator> {
	
	@PersistenceContext( unitName = "IsoPlatformPU" )
    private EntityManager em;

  
	 public EntityManager getEm() {
		return em;
	}




	public void setEm(EntityManager em) {
		this.em = em;
	}




	public IsoAdminServiceImpl() {
	       super(Administrator.class) ;
	    }

	
	
	
	public Administrator authenticate(String login, String password) {
		Administrator found= null;
		try{
			found=(Administrator) em.createNamedQuery("Admin.authenticate").setParameter("param1", login).setParameter("param2", password).getSingleResult();
		}catch(Exception ex){
		}
		return found;
	}

	@SuppressWarnings("unchecked")
	public List<Administrator> findAll() {
		return em.createNamedQuery("Admin.findAll").getResultList();
		
	}




	@Override
	public List<Administrator> findAll(Object param) {
		// TODO Auto-generated method stub
		return null;
	}





	
	

}
