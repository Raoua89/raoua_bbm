package edu.app.iso.business;

import javax.ejb.Remote;

@Remote
public interface EmailSenderRemote {
	public void sendEMail(String sender, String receiver,String text);

}
