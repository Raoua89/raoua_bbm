package edu.app.iso.business;



import java.util.List;

import javax.ejb.Remote;
import edu.app.iso.persistence.QCM;



@Remote
public interface QcmServiceRemote extends AbstractFacadeRemote<QCM> {

	List<QCM> findAll();

	void CalculFinalScore(QCM qcm);

	void updateNbQuestion(int nb);
	public int getNbQuestion();
	
	
	
	
	
}
