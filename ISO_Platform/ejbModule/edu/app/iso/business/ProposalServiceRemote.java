package edu.app.iso.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.iso.persistence.Proposal;
import edu.app.iso.persistence.Question;

@Remote
public interface ProposalServiceRemote extends AbstractFacadeRemote<Proposal> {

	List<Proposal> findAllProposals();

	List<Proposal> findProposals(Question question);

}
