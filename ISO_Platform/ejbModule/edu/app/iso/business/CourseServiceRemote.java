package edu.app.iso.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.iso.persistence.Course;

@Remote
public interface CourseServiceRemote extends AbstractFacadeRemote<Course>{

	List<Course> findAll();

	List<Course> findByBusinessArea(String bus);

	Course findByTitle(String title);

}
