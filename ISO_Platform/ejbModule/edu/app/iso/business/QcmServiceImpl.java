package edu.app.iso.business;


import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.iso.persistence.QCM;
import edu.app.iso.persistence.Score;

/**
 * Session Bean implementation class QcmServiceImpl
 */
@Stateless(name="QcmService")
public class QcmServiceImpl extends AbstractFacade<QCM> implements QcmServiceRemote {
 
	@PersistenceContext( unitName = "IsoPlatformPU" )
	private EntityManager em;
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public QcmServiceImpl() {
		super(QCM.class);
	}

	@SuppressWarnings("unchecked")
	public List<QCM> findAll() {
		 try{
			 return em.createNamedQuery("QCM.findAll").getResultList();
		 } catch(Exception e)
		 {return null;}
		  
    }
	


	public void CalculFinalScore(QCM qcm) {
		for(Score score : qcm.getScores())
		{qcm.setFinalScore(qcm.getFinalScore()+score.getValue());
		}
		
	}

	
	public void updateNbQuestion(int nb) {
		QCM.setNbQuestions(nb);
		
	}
	
	public int getNbQuestion() {
		return QCM.getNbQuestions();
		
	}

	

	

	
    

}
