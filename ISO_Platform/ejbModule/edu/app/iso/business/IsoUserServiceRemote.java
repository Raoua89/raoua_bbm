package edu.app.iso.business;

import java.util.List;

import javax.ejb.Remote;




@Remote
public interface IsoUserServiceRemote<T> extends AbstractFacadeRemote<T> {
	
	
	public T authenticate(String login, String password);

	public List<T> findAll();

	public List<T> findAll(Object param);

	
	
}