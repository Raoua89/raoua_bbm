package edu.app.iso.business;



import java.util.List;

import javax.ejb.Remote;


import edu.app.iso.persistence.Enterprise;

@Remote
public interface IsoEnterpriseServiceRemote {
	void createEnterprise(Enterprise enterprise);
	Enterprise findEnterprise(int id);
	public Enterprise findEnterpriseByName(String name);
	void updateEnterprise(Enterprise enterprise);
	void removeEnterprise(Enterprise enterprise);
	List<Enterprise> findAllEnterprises();

}
