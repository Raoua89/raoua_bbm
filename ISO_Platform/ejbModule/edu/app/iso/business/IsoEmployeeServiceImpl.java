package edu.app.iso.business;


import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.iso.persistence.Employee;


/**
 * Session Bean implementation class isoService
 */
@Stateless(name="EmployeeService")
public class IsoEmployeeServiceImpl  extends AbstractFacade<Employee> implements IsoUserServiceRemote<Employee> {
	
	@PersistenceContext( unitName = "IsoPlatformPU" )
    private EntityManager em;
	

    public EntityManager getEm() {
		return em;
	}


	public void setEm(EntityManager em) {
		this.em = em;
	}


	/**
     * Default constructor. 
     */
    public IsoEmployeeServiceImpl() {
       super(Employee.class) ;
    }
    
   
  @SuppressWarnings("unchecked")
	public List<Employee> findAll(Object param) {
	       
    	List<Employee> result=new ArrayList<Employee>();
		 try{
		 result=em.createNamedQuery("Employee.findAll").setParameter("param", param).getResultList();
		 } catch(Exception e)
		 {}
		 return result;
    }

	 public Employee authenticate(String login, String password) {
			Employee found= null;
			try{
				found=(Employee) em.createNamedQuery("Employee.authenticate").setParameter("param1", login).setParameter("param2", password).getSingleResult();
			}catch(Exception ex){
			}
			return found;
		 
		}


	@SuppressWarnings("unchecked")
	public List<Employee> findAll() {
		return (List<Employee>) em.createNamedQuery("Employee.findAllEmployees").getResultList();
	}


	


	
	

}
