package edu.app.iso.business;

import java.util.List;
import java.util.Random;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.iso.persistence.Course;
import edu.app.iso.persistence.QCM;
import edu.app.iso.persistence.Question;
import edu.app.iso.persistence.Response;

/**
 * Session Bean implementation class QuestionServiceImpl
 */
@Stateless(name="QuestionService")
public class QuestionServiceImpl extends AbstractFacade<Question> implements QuestionServiceRemote {
	@PersistenceContext( unitName = "IsoPlatformPU" )
   private EntityManager em;
	
    public EntityManager getEm() {
	return em;
}

public void setEm(EntityManager em) {
	this.em = em;
}

	public QuestionServiceImpl() {
       super(Question.class);
    }
    
    @SuppressWarnings("unchecked")
	public List<Question> findQuestions(Course course, String dificulty){
    	try{
    	return em.createNamedQuery("Question.findAll").setParameter("param1", course.getCourseId()).setParameter("param2", dificulty).getResultList();
    	}catch(Exception ex){}
    	return null;
    }
    public Question chooseQuestion(List<Question> list){
    	Random r = new Random();
    	int range = r.nextInt(list.size()+1);
    	return list.get(range);
    }

	
	@SuppressWarnings("unchecked")
	public List<Question> findAllQuestions(Course course) {
		
		return em.createNamedQuery("Question.findAllQuestions").setParameter("param",course).getResultList();
	}

	
	public Response fetchResponse(Question question, QCM qcm) {
		
		return (Response) em.createNamedQuery("Question.fetch").setParameter("param1", qcm).setParameter("param2", question).getSingleResult();
	}

	    
    
}
