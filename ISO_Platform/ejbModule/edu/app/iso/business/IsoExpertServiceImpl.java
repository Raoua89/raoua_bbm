package edu.app.iso.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.iso.persistence.Expert;

/**
 * Session Bean implementation class isoExpertService
 */
@Stateless(name="ExpertService")
public class IsoExpertServiceImpl extends AbstractFacade<Expert> implements IsoUserServiceRemote<Expert> {
	
	@PersistenceContext( unitName = "IsoPlatformPU" )
    private EntityManager em;


    public EntityManager getEm() {
		return em;
	}




	public void setEm(EntityManager em) {
		this.em = em;
	}




	/**
     * Default constructor. 
     */
	 public IsoExpertServiceImpl() {
	       super(Expert.class) ;
	    }

	
	
	
	public Expert authenticate(String login, String password) {
		Expert found= null;
		try{
			found=(Expert) em.createNamedQuery("Expert.authenticate").setParameter("param1", login).setParameter("param2", password).getSingleResult();
		}catch(Exception ex){
		}
		return found;
	}




	@SuppressWarnings("unchecked")
	public List<Expert> findAll() {
		return em.createNamedQuery("Expert.findAll").getResultList();
		
	}




	
	public List<Expert> findAll(Object param) {
		
		return null;
	}





	

}
